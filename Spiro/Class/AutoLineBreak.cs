﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Spiro.Class
{
    /// <summary>
    /// Stores the width of each character. For auto-line breaking.
    /// 
    /// A lot of logic is borrowed from TextParsing class.
    /// </summary>

    public class WidthStruct
    {
        public WidthStruct()
        {
            CHRLengthlookup = new(new ByteComparer());
            ControlCodeLengthlookup = new(new ByteComparer());
            DictionaryKeys = new();
        }

        /// <summary>
        /// Key     = CHR hex value
        /// Value   = length
        /// </summary>
        /// 
        public SortedDictionary<byte[], int> CHRLengthlookup { get; private set; }
        public SortedDictionary<byte[], int> ControlCodeLengthlookup { get; private set; }
        public Queue<byte[]> DictionaryKeys { get; private set; }
    }

    public sealed partial class WidthCalc
    {
        // Key 1: CHR
        // Key 2: Dictionary
        private Dictionary<(string, string), WidthStruct> Widths { get; set; }
        private string LineBreakChar { get; set; }

        private string CurrentCHRFilename { get; set; }
        private string CurrentDictionaryFilename { get; set; }

        public WidthCalc(string lineBreakChar)
        {
            Widths = new();
            LineBreakChar = lineBreakChar;
        }

        public void LoadCHR(string currentCHRFilename, string currentDictionaryFilename)
        {
            CurrentCHRFilename = currentCHRFilename;
            CurrentDictionaryFilename = currentDictionaryFilename;

            if (!Widths.ContainsKey((CurrentCHRFilename, CurrentDictionaryFilename)))
            {
                Widths.Add((CurrentCHRFilename, CurrentDictionaryFilename), new());
            }

            void LengthLookupRead(ref SortedDictionary<byte[], int> lengthLookup, string fileName)
            {
                // Check if we read the CHR length table file before
                if (lengthLookup.Count > 0)
                {
                    return;
                }

                var currentChrFormatted = string.Format(Constants.Path_To_x_length, fileName);
                if (!File.Exists(currentChrFormatted))
                {
                    return;
                    //throw new Exception($"File doesn't exist: {currentChrFormatted}");
                }

                using FileStream inputOpenedFile = File.Open(currentChrFormatted, FileMode.Open);
                using StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8);

                var split = sr.ReadToEnd().Split(Constants.txtNewline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string fileLine in split)
                {
                    if (fileLine.StartsWith("//"))
                    {
                        continue;
                    }

                    byte[] hexValue = GetHexValueFromFileLine(fileLine);
                    int length = ParseLengthFileLine(fileLine);

                    if (lengthLookup.ContainsKey(hexValue))
                    {
                        throw new Exception($"Duplicate key. {fileName}. Key: {MyMath.FormatBytesInBrackets(hexValue)}");
                    }

                    lengthLookup.Add(hexValue, length);
                }
            }

            var temp1 = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup;
            LengthLookupRead(ref temp1, currentDictionaryFilename);

            var temp2 = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup;
            LengthLookupRead(ref temp2, currentCHRFilename);
        }

        public void LoadDictionaryTemplate(string currentCHRFilename, string currentDictionaryFilename)
        {
            CurrentCHRFilename = currentCHRFilename;
            CurrentDictionaryFilename = currentDictionaryFilename;

            if (!Widths.ContainsKey((CurrentCHRFilename, CurrentDictionaryFilename)))
            {
                Widths.Add((CurrentCHRFilename, CurrentDictionaryFilename), new());
            }

            // Check if we read the dictionary table file before
            if (Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].DictionaryKeys.Count > 0)
            {
                return;
            }

            var CurrentDictionaryFilenameFormatted = MyDictionary.GenerateFilename(false, currentDictionaryFilename);
            using FileStream inputOpenedFile = File.Open(CurrentDictionaryFilenameFormatted, FileMode.Open);
            using StreamReader sr = new StreamReader(inputOpenedFile, Encoding.UTF8);

            foreach (string fileLine in sr.ReadToEnd().Split(Constants.txtNewline.ToCharArray(), StringSplitOptions.RemoveEmptyEntries))
            {
                if (fileLine.StartsWith("//"))
                {
                    continue;
                }
                
                byte[] hexValue = GetHexValueFromFileLine(fileLine);
                Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].DictionaryKeys.Enqueue(hexValue);
            }
        }

        public string TokimekiMihoTransAddLineBreaks(string subentryId, string dialogueEntry, int pixelsPerLine, MyDictionary chara, bool autoEdgeLinebreak)
        {
            //Split the dialogue so my control codes are in one index while the dialogue is 
            string[] spiroControlCode = dialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            dialogueEntry = Regex.Replace(dialogueEntry, @"\\.*?/", "");

            /*
             * Parse my own control codes.
             * LineWidth = Skips the linebreaking code entirely
             * LineBreakSkip = let's you set the width of the textbox
             */

            bool IsWhiteSpace(string s)
            {
                return s == " " || s == "  ";
            }

            bool overrideLineSkip = false;
            string newScriptEntry = "";

            if (spiroControlCode.Length > 1)
            {
                foreach (string s in spiroControlCode)
                {
                    if (s.StartsWith("LineWidth"))
                    {
                        pixelsPerLine = int.Parse(s.Split('=')[1]);
                    }
                    else if (s.Equals("LineBreakSkip"))
                    {
                        newScriptEntry = dialogueEntry;
                        overrideLineSkip = true;
                        break;
                    }
                }
            }

            if (!overrideLineSkip)
            {
                /*
                 * Split the dialogue string into chunks that we can work with.
                 * Space get their own entries.
                 * 
                 * Control codes have their own index
                 * Bytes get their own index
                 * And words like "Cat" get their own index. 
                 * Characters like [...] are part of a word. So you'll 
                 * get indexes that have "Hmm[...].
                 */
                var escaped = Regex.Escape(LineBreakChar);
                string[] a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})|(\<.*?\>)|(" + escaped + @")|(\[NEXT\])|(\[STOP\])")).Split(dialogueEntry).Where(s => s != String.Empty).ToArray();

                int[] ChunkLength = new int[a_chunks.Length];

                for (int i = 0; i < a_chunks.Length; i++)
                {
                    string ChunkToInsert = a_chunks[i];

                    if (ChunkToInsert[0] == Constants.ACTION1)
                    {
                        //determine how much space a controlcode takes up.
                        ChunkLength[i] += Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup[chara.DictSortedByString[ChunkToInsert].ByKey.ElementAt(0).Key];
                    }
                    else if (ChunkToInsert[0] == '<')
                    {
                        ChunkLength[i] += 0;
                    }
                    else if (ChunkToInsert[0] != Constants.BYTE1)
                    {
                        /*
                         * Have each index in SubChunk contains a single character.
                         * Stuff in brackets count as a single character. 
                         * 
                         * For example:
                         * 
                         * Full word: "Hmm[...]"
                         * 
                         * Becomes:
                         * 
                         * [0]=H
                         * [1]=m
                         * [2]=m
                         * [3]=[...]
                         * 
                         * This makes the code simple. 
                         */
                        string[] a_SubChunk = (new Regex(@"(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                        foreach (string CharacterToGetWidthOf in a_SubChunk)
                        {
                            if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                            {
                                if (Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup.ContainsKey(hexid))
                                {
                                    ChunkLength[i] += Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup[hexid];
                                }
                                else
                                {
                                    throw new Exception($"Length calc lookup key does not exist: {MyMath.DecToHex(hexid)}. Character: {CharacterToGetWidthOf}");
                                }
                            }
                            else
                            {
                                throw new Exception($"{nameof(subentryId)} {subentryId}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                            }
                        }
                    }
                }

                chara.GetCHRByteValue(Constants.SPACE, out byte[] spaceId);

                if (!Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup.ContainsKey(spaceId))
                {
                    throw new Exception("No length for space in table file.");
                }

                int WidthOfSpace = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup[spaceId];

                const int WidthOfLineBreak = 0;

                int originalPixelsPerLine = pixelsPerLine;
                bool willLineBreak = false;
                int lineLength = 0;
                bool delayedSpaceInsertion = false;

                const int maxLineNo = 8;
                int currentLine = 1;

                bool newTextboxArea = false;
                bool previousEdgeCase = false;

                //Match on control codes and byte data.
                Regex regex = new Regex(@"(\(.*?\))|({.*?})");

                for (int i = 0; i < a_chunks.Length; i++)
                {
                    void IncrementCurrentLine()
                    {
                        currentLine++;
                        if (currentLine == maxLineNo)
                        {
                            currentLine = 1;
                        }

                        if (currentLine == 7)
                        {
                            pixelsPerLine = originalPixelsPerLine - 8; //to adjust for cursor
                        }
                        else
                        {
                            pixelsPerLine = originalPixelsPerLine;
                        }
                    }

                    string chunkToInsert = a_chunks[i];

                    if (chunkToInsert == LineBreakChar)
                    {
                        if (previousEdgeCase)
                        {
                            previousEdgeCase = false;
                            continue;
                        }

                        willLineBreak = true;
                        delayedSpaceInsertion = false;
                    }
                    else if (chunkToInsert == "[NEXT]")
                    {
                        lineLength = 0;
                    }
                    else if (chunkToInsert == "(PAGE)")
                    {
                        lineLength = 0;
                        currentLine = 0;
                        pixelsPerLine = originalPixelsPerLine;
                    }

                    previousEdgeCase = false;

                    bool edgeCase = (lineLength + ChunkLength[i] == pixelsPerLine) && !newTextboxArea && autoEdgeLinebreak;

                    if ((lineLength + ChunkLength[i] > pixelsPerLine) || edgeCase)
                    {
                        willLineBreak = true;
                    }

                    //Remove a space at the end of a line if we're linebreaking.
                    if (willLineBreak && !edgeCase && i > 0 && a_chunks[i - 1] == Constants.SPACE && newScriptEntry.Length > 0)
                    {
                        newScriptEntry = newScriptEntry[0..^1];
                        lineLength -= WidthOfSpace;
                    }

                    if (willLineBreak && !edgeCase && i > 0 && a_chunks[i - 1] == "  ")
                    {
                        newScriptEntry = newScriptEntry[0..^2];
                        lineLength -= WidthOfSpace * 2;
                    }

                    if (!willLineBreak && autoEdgeLinebreak && !IsWhiteSpace(chunkToInsert) && edgeCase)
                    {
                        willLineBreak = true;
                    }

                    if (willLineBreak)
                    {
                        if (!(edgeCase))
                        {
                            if (newScriptEntry.EndsWith(" ") || newScriptEntry.EndsWith("  ") && WidthOfLineBreak == 0)
                            {
                                newScriptEntry = newScriptEntry.TrimEnd();
                                lineLength -= WidthOfSpace;
                            }
                        }

                        /*
                         * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                         */
                        int j = i - 1;
                        while (++j < a_chunks.Length && regex.Match(a_chunks[j]).Success && a_chunks[j] != LineBreakChar) { }

                        //Game already autolinebreaks in right on the edge.
                        if (autoEdgeLinebreak && edgeCase)
                        {
                            willLineBreak = false;
                            lineLength = -ChunkLength[i];
                            IncrementCurrentLine();
                            previousEdgeCase = true;
                        }
                        else if (j < a_chunks.Length)
                        {
                            if (!newTextboxArea)
                            {
                                newScriptEntry = string.Concat(newScriptEntry, LineBreakChar);
                                lineLength += WidthOfLineBreak;
                                IncrementCurrentLine();
                            }
                            else
                            {
                                newTextboxArea = false;
                            }

                            lineLength = 0;
                        }

                        willLineBreak = false;
                    }

                    if (chunkToInsert != LineBreakChar)
                    {
                        /*
                         * Delaying the space insertion when the next character has a length of 0 to
                         * makes it easier to determine when we have a trailing space at the end of a line.
                         * 
                         * For example, this is easier to determine that
                         * we need to remove an extra space:
                         * "(CONTROL CODE) "
                         * 
                         * This is not:
                         * " (CONTROL CODE)"
                         */

                        if (chunkToInsert == " " && i + 1 < ChunkLength.Length && ChunkLength[i] == 0)
                        {
                            delayedSpaceInsertion = true;
                        }
                        else
                        {
                            //Not sure why there's two white space removers (one above), but I'll leave this alone now.
                            if (!(lineLength == 0 && IsWhiteSpace(chunkToInsert)))
                            {
                                newScriptEntry = string.Concat(newScriptEntry, chunkToInsert);
                                lineLength += ChunkLength[i];
                            }

                            if (delayedSpaceInsertion && i + 1 < ChunkLength.Length && !regex.Match(a_chunks[i + 1]).Success)
                            {
                                newScriptEntry = string.Concat(newScriptEntry, Constants.SPACE);
                                lineLength += WidthOfSpace;
                                delayedSpaceInsertion = false;
                            }
                        }
                    }
                }
            }

            if (newScriptEntry.Contains('<') || newScriptEntry.Contains('>'))
            {
                throw new Exception("Script cannot contain < or > after autolinebreaking. Something's off.");
            }

            return newScriptEntry;
        }

        public string MetalSladerTransAddLineBreaks(string subentryId, string DialogueEntry, int PixelsPerLine, MyDictionary chara, bool InGameAutotLineBreak = false)
        {
            //Split the dialogue so my control codes are in one index while the dialogue is 
            string[] SpiroControlCode = DialogueEntry.Split(new char[] { '\\', '/' }, StringSplitOptions.RemoveEmptyEntries);
            DialogueEntry = Regex.Replace(DialogueEntry, @"\\.*?/", "");

            /*
             * Parse my own control codes.
             * LineWidth = Skips the linebreaking code entirely
             * LineBreakSkip = let's you set the width of the textbox
             */

            bool OverrideLineSkip = false;
            string NewScriptEntry = "";
            bool portraitAlreadyOpen = false;

            if (SpiroControlCode.Length > 1)
            {
                foreach (string s in SpiroControlCode)
                {
                    if (s.StartsWith("LineWidth="))
                    {
                        PixelsPerLine = int.Parse(s.Split('=')[1]);
                    }
                    else if (s.Equals("LineBreakSkip"))
                    {
                        NewScriptEntry = DialogueEntry;
                        OverrideLineSkip = true;
                        break;
                    }
                    else if (s.Equals("LineWidthPortraitShowing"))
                    {
                        portraitAlreadyOpen = true;
                        break;
                    }
                }
            }

            if (!OverrideLineSkip)
            {
                const string
                    CODE_PORTRAIT_OPEN = "(CODE 10 Portrait)",
                    CODE_PORTRAIT_CLOSE = "(CODE 11)",
                    CODE_ENDQUOTE = "(End quote)",
                    CODE_WAIT = "(WAIT)",
                    CODE_NAMECARD_COLON = ":";

                const int LENGTH_OF_CURSOR = 8;
                const int LENGTH_OF_PORTRAINT = 64;

                /*
                 * Split the dialogue string into chunks that we can work with.
                 * Space get their own entries.
                 * 
                 * Control codes have their own index
                 * Bytes get their own index
                 * And words like "Cat" get their own index. 
                 * Characters like [...] are part of a word. So you'll 
                 * get indexes that have "Hmm[...].
                 */
                string[] a_chunks;
                if (InGameAutotLineBreak)
                {
                    a_chunks = (new Regex(@"(\((?!End quote).*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
                }
                else
                {
                    a_chunks = (new Regex(@"(\(.*?\))|(\s+(?![^\(]*\))(?![^\[]*\]))|({.*?})")).Split(DialogueEntry).Where(s => s != String.Empty).ToArray();
                }

                int[] a_chunkLength = new int[a_chunks.Length];

                for (int i = 0; i < a_chunks.Length; i++)
                {
                    string ChunkToInsert = a_chunks[i];

                    if (ChunkToInsert[0] == Constants.ACTION1)
                    {
                        if (!chara.DictSortedByString.ContainsKey(ChunkToInsert))
                        {
                            throw new Exception($"The following is not a valid control code: {ChunkToInsert}.");
                        }

                        //determine how much space a controlcode takes up.
                        byte[] id = chara.DictSortedByString[ChunkToInsert].ByKey.ElementAt(0).Key;

                        if (!Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup.ContainsKey(id))
                        {
                            throw new Exception($"The following control code doesn't have a length: {ChunkToInsert}.");
                        }

                        a_chunkLength[i] = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup[id];
                    }
                    else if (ChunkToInsert[0] != Constants.BYTE1)
                    {
                        /*
                         * Have each index in SubChunk contains a single character.
                         * Stuff in brackets count as a single character. 
                         * 
                         * For example:
                         * 
                         * Full word: "Hmm[...]"
                         * 
                         * Becomes:
                         * 
                         * [0]=H
                         * [1]=m
                         * [2]=m
                         * [3]=[...]
                         * 
                         * This makes the code simple. 
                         */
                        string[] a_SubChunk = (new Regex(@"(\(.*?\))|(\[.*?\])|(.)")).Split(ChunkToInsert).Where(s => s != string.Empty).ToArray();

                        foreach (string CharacterToGetWidthOf in a_SubChunk)
                        {
                            if (CharacterToGetWidthOf[0] == Constants.ACTION1)
                            {
                                if (!chara.DictSortedByString.ContainsKey(CharacterToGetWidthOf))
                                {
                                    throw new Exception($"The following is not a valid control code: {CharacterToGetWidthOf}.");
                                }

                                //determine how much space a controlcode takes up.
                                a_chunkLength[i] += Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup[chara.DictSortedByString[CharacterToGetWidthOf].ByKey.ElementAt(0).Key];
                            }
                            else if (chara.GetCHRByteValue(CharacterToGetWidthOf, out byte[] hexid))
                            {
                                if (!Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup.ContainsKey(hexid))
                                {
                                    throw new Exception($"No length entry for key: {MyMath.FormatByteInBrackets(hexid)}");
                                }

                                a_chunkLength[i] += Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup[hexid];
                            }
                            else
                            {
                                throw new Exception($"{nameof(subentryId)} {subentryId}. Unable to insert the following string: \"{CharacterToGetWidthOf}\".");
                            }
                        }
                    }
                }


                chara.GetCHRByteValue(Constants.SPACE, out byte[] space_id);
                int WidthOfSpace = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup[space_id];

                int CurrentLine = 1;
                int OriginalPixelsPerLine = PixelsPerLine;
                bool WillLineBreak = false;
                int LineLength = 0;
                bool DelayedSpaceInsertion = false;
                bool WaitFlag = false;
                bool PortraitDisplay = false;
                bool AdjustForEndquote = false;
                bool LastWordChunk = false;
                int LastChunkLength = 0;

                //Match on control codes and byte data.

                Regex RegexSplit;

                if (InGameAutotLineBreak)
                {
                    RegexSplit = new Regex(@"(\((?!End quote).*?\))|({.*?})");
                }
                else
                {
                    RegexSplit = new Regex(@"(\(.*?\))|({.*?})");
                }

                for (int i = 0; i < a_chunks.Length; i++)
                {
                    if (portraitAlreadyOpen)
                    {
                        PortraitDisplay = true;
                        PixelsPerLine = GetLengthOfTextbox();
                        portraitAlreadyOpen = false;
                    }

                    int GetLengthOfTextbox()
                    {
                        int TextboxLength = OriginalPixelsPerLine;

                        /*
                         * The wait cursor decrement doesn't stack with the portrait 
                         * length decrement.
                         * 
                         * You see, you can't actually write in the column where the cursor
                         * would be when a portrait is displayed.
                         */
                        if (PortraitDisplay)
                        {
                            TextboxLength = OriginalPixelsPerLine - LENGTH_OF_PORTRAINT;
                        }
                        else if (WaitFlag)
                        {
                            TextboxLength = OriginalPixelsPerLine - LENGTH_OF_CURSOR;
                        }

                        if (AdjustForEndquote)
                        {
                            TextboxLength -= 8;
                        }


                        return TextboxLength;
                    }

                    string chunkToInsert = a_chunks[i];

                    if (InGameAutotLineBreak && (LineLength == 0 && i != 0 && (chunkToInsert == Constants.SPACE || chunkToInsert == Constants.NEWLINE)))
                    {
                        continue;
                    }

                    LastWordChunk = false;
                    LastChunkLength = 0;

                    /*
                     * Did we process the wait control code?
                     * If we did, set the textbox width back,
                     * we are no longer factoring in the "wait cursor"
                     */
                    if (WaitFlag)
                    {
                        WaitFlag = false;
                        PixelsPerLine = GetLengthOfTextbox();
                    }

                    if (AdjustForEndquote)
                    {
                        AdjustForEndquote = false;
                        PixelsPerLine = GetLengthOfTextbox();
                    }

                    if (!WaitFlag && CurrentLine > 3)
                    {
                        //Look ahead and see if there's a (WAIT) control code ahead.
                        int j = i;
                        while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && a_chunks[j] != CODE_WAIT && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }

                        if (j < a_chunks.Length && a_chunks[j] == CODE_WAIT)
                        {
                            //Reduce the max number of pixels per line by the arrows width.
                            //Otherwise the text would overlap with the next arrow.
                            WaitFlag = true;
                            PixelsPerLine = GetLengthOfTextbox();
                        }
                    }

                    /*
                     * Do we have the have's own autolinebreaking code enabled.
                     * (The code sucks and actually restricts how 
                     * much text you can have onscreen)
                     */

                    if (InGameAutotLineBreak)
                    {
                        if (chunkToInsert.Contains(CODE_ENDQUOTE))
                        {
                            AdjustForEndquote = true;
                            PixelsPerLine = GetLengthOfTextbox();
                        }
                        else if (!chunkToInsert.Contains(Constants.SPACE))
                        {
                            int j = i;
                            while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_ENDQUOTE) && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE) { }


                            if (j < a_chunks.Length && a_chunks[j].Contains(CODE_ENDQUOTE))
                            {
                                AdjustForEndquote = true;
                                PixelsPerLine = GetLengthOfTextbox();
                            }
                        }
                    }

                    if (chunkToInsert[0] == Constants.ACTION1)
                    {
                        if (chunkToInsert.Contains(CODE_NAMECARD_COLON) || chunkToInsert.Contains(CODE_ENDQUOTE))
                        {
                            LineLength = 0;
                            DelayedSpaceInsertion = false;
                        }

                        if (chunkToInsert.Contains(CODE_ENDQUOTE))
                        {
                            CurrentLine++;
                        }

                        if (chunkToInsert == Constants.NEWLINE)
                        {
                            LineLength = 0;
                            CurrentLine++;
                            DelayedSpaceInsertion = false;
                        }

                        if (chunkToInsert == CODE_PORTRAIT_OPEN)
                        {
                            PortraitDisplay = true;
                            PixelsPerLine = GetLengthOfTextbox();
                            CurrentLine = 1;
                        }
                        else if (chunkToInsert == CODE_PORTRAIT_CLOSE)
                        {
                            PortraitDisplay = false;
                            PixelsPerLine = GetLengthOfTextbox();
                            CurrentLine = 1;
                        }
                    }

                    /*
                     * Check if a word is split by a control code.
                     * 
                     * If it is, see if we need to linebreak.
                     */
                    if (!WillLineBreak && chunkToInsert != Constants.SPACE && chunkToInsert[0] != Constants.ACTION1 && chunkToInsert[0] != Constants.BYTE1)
                    {
                        int j = i - 1;

                        int LengthOfFullWord = 0;
                        while (++j < a_chunks.Length && a_chunks[j] != Constants.SPACE && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE)
                        {
                            LengthOfFullWord += a_chunkLength[j];
                        }

                        if (j == a_chunks.Length)
                        {
                            LastWordChunk = true;
                            LastChunkLength = LengthOfFullWord;
                        }


                        if (LengthOfFullWord > a_chunkLength[i])
                        {
                            if (LineLength + LengthOfFullWord > PixelsPerLine)
                            {
                                LineLength = 0;
                                WillLineBreak = true;
                            }
                        }
                    }

                    if (!WillLineBreak)
                    {
                        int u = i;

                        if (!RegexSplit.Match(a_chunks[u]).Success)
                        {
                            //Check to see if we didn't already identify 
                            //it's the last chunk previously in function (when we were checking for a word 
                            //split by control code(s).
                            if (!LastWordChunk)
                            {
                                LastChunkLength += a_chunkLength[u];

                                while (++u < a_chunks.Length && RegexSplit.Match(a_chunks[u]).Success && a_chunks[u] != Constants.SPACE && a_chunks[u] != Constants.NEWLINE)
                                {
                                    LastChunkLength += a_chunkLength[u];
                                }

                                if (u == a_chunks.Length)
                                {
                                    LastWordChunk = true;
                                }
                            }
                        }

                        bool NormalLineBreak = (LineLength + a_chunkLength[i] > PixelsPerLine);

                        //Kind of a bad way to check if this is the final chunk with
                        //text to insert.
                        if (!NormalLineBreak && (CurrentLine >= 3 && !WaitFlag && LastWordChunk && !PortraitDisplay))
                        {
                            if (LineLength + LastChunkLength > PixelsPerLine - LENGTH_OF_CURSOR)
                            {
                                if (Global.Debug)
                                {
                                    Console.WriteLine($"Autolinebreak: end of line; {nameof(subentryId)}: {subentryId}.");
                                }
                                WillLineBreak = true;
                            }
                        }
                        else
                        {
                            if (NormalLineBreak)
                            {
                                WillLineBreak = true;
                            }
                        }
                    }

                    //Remove a space at the end of a line if we're linebreaking.
                    if (WillLineBreak && NewScriptEntry.Length > 0 && NewScriptEntry[^1].ToString() == Constants.SPACE)
                    {
                        int j = i;
                        while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && a_chunks[j] != Constants.NEWLINE) { }

                        if (j < a_chunks.Length && a_chunks[j] == Constants.NEWLINE)
                        {
                            NewScriptEntry = NewScriptEntry[0..^1];
                            LineLength -= WidthOfSpace;
                        }
                    }

                    if (WillLineBreak)
                    {
                        if (NewScriptEntry.EndsWith(Constants.SPACE))
                        {
                            NewScriptEntry = NewScriptEntry.TrimEnd();
                        }

                        /*
                         * Don't add a linebreak if there's nothing ahead but control codes at the end of the speaker's line.
                         */
                        int j = i - 1;
                        while (++j < a_chunks.Length && RegexSplit.Match(a_chunks[j]).Success && !a_chunks[j].Contains(CODE_NAMECARD_COLON) && a_chunks[j] != Constants.NEWLINE && a_chunks[j].Contains(CODE_ENDQUOTE)) { }

                        if (j < a_chunks.Length && !a_chunks[j].Contains(CODE_NAMECARD_COLON))
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, Constants.NEWLINE);
                            LineLength = 0;
                            CurrentLine++;
                        }

                        WillLineBreak = false;
                    }

                    if (InGameAutotLineBreak && (LineLength + a_chunkLength[i] == PixelsPerLine))
                    {
                        LineLength = 0 - a_chunkLength[i];
                    }

                    if (!(LineLength == 0 && chunkToInsert == Constants.SPACE))
                    {
                        /*
                         * Delaying the space insertion makes it easier to determine when
                         * we have a trailing space at the end of a line.
                         * 
                         * For example, this is easier to determine that
                         * we need to remove an extra space:
                         * "(CONTROL CODE) "
                         * 
                         * This is not:
                         * " (CONTROL CODE)"
                         */

                        if (chunkToInsert == " " && i + 1 < a_chunkLength.Length && a_chunkLength[i] == 0)
                        {
                            DelayedSpaceInsertion = true;
                        }
                        else
                        {
                            NewScriptEntry = string.Concat(NewScriptEntry, chunkToInsert);
                            LineLength += a_chunkLength[i];

                            if (DelayedSpaceInsertion && i + 1 < a_chunkLength.Length && !RegexSplit.Match(a_chunks[i + 1]).Success)
                            {
                                NewScriptEntry = string.Concat(NewScriptEntry, Constants.SPACE);
                                LineLength += WidthOfSpace;
                                DelayedSpaceInsertion = false;
                            }

                            if (chunkToInsert.Contains(CODE_ENDQUOTE))
                            {
                                LineLength = 0;
                                DelayedSpaceInsertion = false;
                            }
                        }
                    }
                }
            }

            return NewScriptEntry;
        }

        /// <summary>
        /// Gets the pixel length for dictionary values. Useful only for control codes that have text behind them.
        /// 
        /// It writes non-control code entries as well. So some clockcycle are wasted.
        /// 
        /// Keep in mind, the text in the control codes can't be broken up between lines.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="chara"></param>
        public void AddDictionaryEntryLength(string line, MyDictionary chara)
        {
            int lengthOfDictionary = 0;
            line = new Regex("{.*?}").Replace(line, "");

            for (int i = 0; i < line.Length; i++)
            {
                string Character;
                int length = 0;
                byte[] possibleKey;
                do
                {
                    if (++length > line.Length - i)
                    {
                        throw new Exception();
                    }

                    Character = line.Substring(i, length);
                }
                while (!chara.GetCHRByteValue(Character, out possibleKey));

                if (!Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup.ContainsKey(possibleKey))
                {
                    if (Global.Verbose)
                    {
                        Console.WriteLine($"CHR key not in length list: $#{MyMath.GetValueOfBytes(possibleKey):X2}. Make sure the value is in the length file.");
                        Console.WriteLine($"Exitting in case autoline breaking is unused.");
                    }
                    return;
                }

                lengthOfDictionary += Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].CHRLengthlookup[possibleKey];
                i += Character.Length - 1;
            }

            var widthKeys = Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].DictionaryKeys;

            if (widthKeys.Count == 0)
            {
                throw new Exception("Keys exhausted in dictionary. Consider expanding your dictionary table file.");
            }

            var id = widthKeys.Dequeue();
            Widths[(CurrentCHRFilename, CurrentDictionaryFilename)].ControlCodeLengthlookup.Add(id, lengthOfDictionary);
        }

        //From the dictionary class more or less.
        private static byte[] GetHexValueFromFileLine(string Str)
        {
            int Index = Str.IndexOf(Constants.SEPERATOR);
            return MyMath.HexToBytes(Str.Substring(0, Index));
        }

        /// <summary>
        /// The argument a line from the length file.
        /// This file stores the lengths of each 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static int ParseLengthFileLine(string str)
        {
            int index = str.IndexOf(Constants.SEPERATOR);
            // return int.Parse(str.Substring(index + Constants.SEPERATOR.Length).ToString());
            return int.Parse(str[(index + Constants.SEPERATOR.Length)..].ToString());
        }
    }
}