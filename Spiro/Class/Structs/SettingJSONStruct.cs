﻿// Convert json to C# classes
// https://json2csharp.com/
// Convert keys to camelcase
// https://toolslick.com/text/formatter/json
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Spiro.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Structs
{
    public class DisplayReplace
    {
        [JsonProperty(Required = Required.Always)]
        public string Find { get; set; }
        public string Replace { get; set; }
        public string ReplaceOriginal { get; set; }
        public string ReplaceNew { get; set; }
    }

    public class ScriptManager
    {
        public string WriteScript { get; set; }
        public List<DisplayReplace> DisplayReplace { get; set; }
    }

    public class PointerReadWrite
    {
        public bool Disabled { get; set; }
        private string File_ = "";
        public string File { get => File_; set => File_ = value; }

        [JsonProperty(Required = Required.Always)]
        public PointerFormat PointerFormat { get; set; }
        [JsonProperty(Required = Required.Always)]
        public RenderFormat RenderFormat { get; set; }
        public EmbeddedPointer EmbeddedPointer { get; set; }

        public PointerReadWrite Clone()
        {
            return (PointerReadWrite)MemberwiseClone();
        }
    }
    public class Entry
    {
        // Only used for the script output in case the user wants to know where the pointer is located
        public long AllPointerLocations { get; set; }
        public long AllTextLocation { get; set; }
        public long AllLength { get; set; }

        public Entry Clone()
        {
            return (Entry)MemberwiseClone();
        }
    }

    public class EmbeddedPointer
    {
        [JsonProperty(Required = Required.Always)]
        public EmbeddedPointerFormat[] Formats { get; set; }
    }

    public class EmbeddedPointerFormat
    {
        [JsonProperty(Required = Required.Always)]
        public string ControlCode { get; set; }
        public Enums.AddressType AddressType { get; set; }
        public Enums.PointerType PointerType { get; set; }
        [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
        public long EntryNumber { get; set; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long PcDifference { set; get; }
        public bool PcDifferenceExcludePointer { set; get; }
        [JsonProperty(Required = Required.Always)]
        public int PointerLength { get; set; }
        public int RecursionLimit_ = Constants.GENERIC_NULL;
        public int RecursionLimit { get => RecursionLimit_; set => RecursionLimit_ = value; }
        [JsonProperty(Required = Required.Always)]
        public Enums.AddrConversion AddressConversion { set; get; }
        public bool ContinueAfterProcessing { get; set; }
        public string[] CustomPointerFormat { get; set; }
        public bool DontDump { get; set; }

        public EmbeddedPointerFormat Clone()
        {
            return (EmbeddedPointerFormat)MemberwiseClone();
        }
    }

    public class PointerFormat
    {
        private string File_ = "";
        public string File { get => File_; set => File_ = value; }
        [JsonConverter(typeof(SumArrayJsonConverter)), JsonProperty(Required = Required.Always)]
        public long[] Addresses { set; get; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long PcDifference { set; get; }
        [JsonProperty(Required = Required.Always)]
        public Enums.AddrConversion AddressConversion { set; get; }
        [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
        public long EntryNumber { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Enums.PointerType PointerType { get; set; }
        //[JsonProperty(Required = Required.Always)]
        public Enums.PointerGrouping PointerGrouping { get; set; }
        [JsonProperty(Required = Required.Always)]
        public int PointerLength { get; set; }
        public Enums.PlacementStrategy PlacementStrategy { get; set; }
        public string[] CustomPointerFormat { get; set; }
        public long BytesBetween { get; set; }
        public bool IgnoreOutOfBoundPointers { set; get; }
        public string SubentryKeyPrefix { set; get; }

        public PointerFormat Clone()
        {
            return (PointerFormat)MemberwiseClone();
        }
    }

    public class RenderFormat
    {
        private string File_ = "";
        public string File { get => File_; set => File_ = value; }
        public Enums.DialogueReadType DialogueReadType { get; set; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long LengthAtBeginningOfLineDifference { get; set; }
        public string[] Delimiters { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Enums.RenderType RenderType { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Enums.DictionaryTables[] DictionaryInteractions { get; set; }
        [JsonProperty(Required = Required.Always)]
        public bool IsDictionary { get; set; }
        [JsonProperty(Required = Required.Always)]

        public Dictionary<string, TableEntry> Tables { get; set; }

        public KeyValuePair<string, TableEntry> TableMain
        {
            get
            {
                var a = Tables.Where(x => x.Value.IsMain).ToArray();

                if (a.Length != 1)
                {
                    throw new Exception($"Need only one main entry.");
                }

                return a[0];
            }
        }

        public class TableEntry
        {
            public bool IsMain { get; set; }
            public string ChrFile { get; set; }
            public string DictionaryFile { get; set; }
            public bool MirrorBlankToMain { get; set; }
        }

        public int PixelsPerLine { get; set; }
        public Enums.AutoLineBreak AutoLineBreak { get; set; }
        public bool MergeDictionaryAndCHRFile { get; set; }
        public string FindAndReplaceFile { get; set; }
        public Enums.FindAndReplace FindAndReplaceStrategy { get; set; }
        public string[] SpecialPointerFormat { get; set; }
        public bool ReverseTableEndianness { get; set; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long FixedSize { set; get; }
        private long DelimiterCount_ = 1;
        [JsonConverter(typeof(SumJsonConverter))]
        public long DelimiterCount { get => DelimiterCount_; set => DelimiterCount_ = value; }
        private bool IncludeRedirects_ = true;
        public bool IncludeRedirects { set { IncludeRedirects_ = value; } get { return IncludeRedirects_; } }
        internal RenderFormat Clone()
        {
            var r = new RenderFormat
            {
                File = this.File,
                DialogueReadType = this.DialogueReadType,
                RenderType = this.RenderType,
                IsDictionary = this.IsDictionary,
                Tables = this.Tables,
                PixelsPerLine = this.PixelsPerLine,
                AutoLineBreak = this.AutoLineBreak,
                FindAndReplaceFile = this.FindAndReplaceFile,
                FindAndReplaceStrategy = this.FindAndReplaceStrategy,
                ReverseTableEndianness = this.ReverseTableEndianness,
                FixedSize = this.FixedSize,
                DelimiterCount = this.DelimiterCount
            };

            if (this.SpecialPointerFormat != null)
            {
                r.SpecialPointerFormat = (string[])this.SpecialPointerFormat.Clone();
            }

            if (this.DictionaryInteractions != null)
            {
                r.DictionaryInteractions = (Enums.DictionaryTables[])this.DictionaryInteractions.Clone();
            }

            if (this.Delimiters != null)
            {
                r.Delimiters = (string[])this.Delimiters.Clone();
            }

            return r;
        }
    }

    public class Pointers
    {
        [JsonProperty("Read")]
        public Dictionary<string, PointerReadWrite> Read { get; set; }

        [JsonProperty("Write")]
        public Dictionary<string, PointerReadWrite> Write { get; set; }

        [JsonProperty("Read&Write")]
        public Dictionary<string, PointerReadWrite> ReadWrite { get; set; }
    }

    public class WriteableRange
    {
        [JsonProperty(Required = Required.Always)]
        public string File { get; set; }
        [JsonConverter(typeof(SumJsonConverter)), JsonProperty(Required = Required.Always)]
        public long StartAddress { get; set; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long Size { get; set; }
        [JsonConverter(typeof(SumJsonConverter))]
        public long PcDifference { set; get; }
        public Enums.WriteType WriteType { get; set; }
    }

    public class WriteRegion
    {
        [JsonProperty(Required = Required.Always)]
        public string[] EntryOwners { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string[] WriteableAddressRanges { get; set; }
    }

    public class Write
    {
        [JsonProperty(Required = Required.Always)]
        public Dictionary<string, WriteableRange> WriteableRange { get; set; }
        [JsonProperty(Required = Required.Always)]
        public WriteRegion[] WriteRegion { get; set; }
    }

    public class Misc
    {
        // This feature is useful for identifying where text is stored
        // Additinally, it's useful to spot if you still have some more text to dump
        public bool BlankOutTextDataAfterRead { get; set; }
        [JsonConverter(typeof(HexStringToByteJsonConverter))]
        public byte BlankOutByte { get; set; }

        /// <summary>
        /// Determines where to obtain the file to write the new pointers to.
        /// 
        /// When writing pointers, the utility by default writes to the file specified under 
        /// "Write.WriteableRange.$VALUE.File" instead of "Pointers.Write.$VALUE.File".
        /// I'll admit this is logically backwards, but it allows more opportunities to utilize
        /// "Read&Write" instead of "Read" and "Write" which takes up way more space.
        /// 
        /// Setting "GetFileToWritePointerFromPointersField" to true writes pointers specified
        /// in the latter field instead of the former field. This is useful when working with multiple files
        /// where pointers reference different files.
        /// </summary>
        public bool GetFileToWritePointerFromPointersField { get; set; }

        public bool IncludedPointerMetadata { get; set; }

        public bool MirrorOriginalToNewOnDump { get; set; }

        public string LineBreakChar { get; set; }
    }

    public class Script
    {
        [JsonProperty(Required = Required.Always)]
        public string ScriptPath { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Original { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Comment { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool New { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Bytes { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Menu { get; set; }

        [JsonProperty(Required = Required.Always)]
        public bool Proof { get; set; }
    }

    public class Root
    {
        [JsonProperty(Required = Required.Always)]
        public int SpiroVersion { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Dictionary<string, string> Files { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Misc Misc { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Script Scripts { get; set; }
        public ScriptManager ScriptManager { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Pointers Pointers { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Write Write { get; set; }
    }

    public sealed class SumJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => throw new NotImplementedException();
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (Global.Debug)
            {
                Console.WriteLine($"ReadJson: Reading property ${reader.Path}");
            }

            if (reader.Value != null)
            {
                return ProcessHexSumString(reader.Value.ToString());
            }
            return -1;
        }

        public static long ProcessHexSumString(string rawString)
        {
            if (Global.Debug)
            {
                Console.WriteLine($"ProcessHexSumString: rawString: {rawString}");
            }

            if (rawString.Contains("+") || rawString.Contains("-"))
            {
                string[] a_chunks = (new Regex(@"(\+)|(\-)")).Split(rawString).Where(s => s != string.Empty).ToArray();

                long Address = 0;
                bool IsAdd = true;
                foreach (string s in a_chunks)
                {
                    switch (s)
                    {
                        case "+":
                            IsAdd = true;
                            break;
                        case "-":
                            IsAdd = false;
                            break;
                        default:
                            long Multipler = IsAdd ? 1 : -1;
                            Address += Multipler * (long)MyMath.HexToUInt64(s);
                            break;
                    }
                }
                return Address;
            }
            else
            {
                if (MyMath.IsHex(rawString))
                {
                    return (long)MyMath.HexToUInt64(rawString);
                }
                else
                {
                    if (long.TryParse(rawString, out long Result))
                    {
                        return Result;
                    }
                    else
                    {
                        throw new ArgumentException($"Couldn't parse the following string as a long: \"{rawString}\"");
                    }
                }
            }
        }
    }

    public sealed class SumArrayJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => throw new NotImplementedException();
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => throw new NotImplementedException();
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            string[] value = JArray.Load(reader).ToObject<string[]>();


            long[] RetVal = Array.Empty<long>();

            if (value != null)
            {
                RetVal = new long[value.Length];

                for (int i = 0; i < value.Length; i++)
                {
                    RetVal[i] = SumJsonConverter.ProcessHexSumString(value[i]);
                }
            }
            return RetVal;
        }
    }

    public sealed class HexStringToByteJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) => typeof(byte).Equals(objectType);
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X));
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (byte)MyMath.HexToDec((string)reader.Value);
    }

    // Override camelcase converter when serializing so dictionary keys aren't converted to camelcase.
    // https://stackoverflow.com/questions/24143149/keep-casing-when-serializing-dictionaries
    public class CamelCaseExceptDictionaryKeysResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
        {
            JsonDictionaryContract contract = base.CreateDictionaryContract(objectType);
            contract.DictionaryKeyResolver = propertyName => propertyName;
            return contract;
        }
    }
}