﻿using Structs;
using System.Collections.Generic;
using static TextParse.TextParsing;

namespace Spiro.Class.PointerManagement
{
    public sealed class DmpData
    {
        public static int PassNumber { get; set; }

        // Key 1: Text file-id
        // Key 2: Subentry text Location
        //
        // The idea is that you may have multiple strings that reference multiple parts of a string:
        // String #1: Dis village gets[LINE]food from Rodetia.[END]
        // String #2: gets[LINE]food from Rodetia.[END]
        // These could be read in any order.
        //
        // We want to dump the text like so:
        //
        // String #1: Dis village <subentry id="1">gets[LINE]food from Rodetia.[END]
        // String #2: <goto id="1">
        public static readonly SortedDictionary<(string, long), SubentryLocationsValue> SubentryLocations = new();

        public static SortedDictionary<string, DumpSubentryMetadata> SubentrMetadata = new();

        // Key 1: Text file-id
        // Key 2: PointerLocation Location
        // Key 3: TextLocationInitialStart
        // Value: Id
        public static readonly SortedDictionary<(string, long, long), string> PointerLocationToid = new();

        public static readonly SortedDictionary<string, int> SubentryIdCounter = new();
    }

    public class SubentryLocationsValue
    {
        public long ParentAddress { get; set; }
        public string Id { get; set; }
        public bool NotDumped { get; set; }

        // Key: Start address
        // Value: Text entry data
        public SortedDictionary<long, ReadByteStreamReturn> Entries = new();
    }

    public class DumpSubentryMetadata
    {
        // Stores what string ids have been dumped already to JSON
        public int DumpNumber { get; set; }

        /// <summary>
        /// Maps the first subentry to a list of its subentries
        /// </summary>
        public List<string> IdsSubentries = new();

        public EmbeddedPointerFormat EmbeddedPointerFormat { get; set; }

        // Maps id to rbr structure
        public ReadByteStreamReturn IdMapping = new();

    }
}