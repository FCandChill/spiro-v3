﻿using Structs;
using System.Collections.Generic;

namespace Spiro.Class.PointerManagement
{
    public sealed class WriteData
    {
        /// <summary>
        /// Key: Indextable name
        /// </summary>
        public static readonly SortedDictionary<string, Dictionary<string, WriteSubentryMetadata>> IndexTableInfo = new();

        /// <summary>
        /// Key: SubentryId
        /// </summary>
        public static readonly SortedDictionary<string, WriteSubentryMetadata> SubentrMetadata = new();

        /// <summary>
        /// Key: SubentryId
        /// Value: Parent sub entry (Subentry it belongs to).
        /// </summary>
        public static readonly SortedDictionary<string, WriteSubentryMetadata> SubentrLabelMetadata = new();

        /// <summary>
        /// Key: SubentryId Or SubentryLabel
        /// </summary>
        public static readonly SortedDictionary<string, WriteInfo> SubentryWriteLocationsInfo = new();

        /// <summary>
        /// Key: Write region id
        /// Value: bytes used.
        /// </summary>
        public static readonly SortedDictionary<string, int> WriteRegionSpaceUsed = new();

        public sealed class WriteInfo
        {
            public string WriteRangeId { get; }
            public long Location { get; }

            public WriteInfo(string WriteRegionId, long Location)
            {
                this.WriteRangeId = WriteRegionId;
                this.Location = Location;
            }
        }

        public sealed class WriteSubentryMetadata
        {
            public string Id { get; set; }
            public string IndexTable { get; set; }
            public long PointerTableLocation { get; set; }
            public int EntryNo { get; set; }
            public int SubentryIndex { get; set; }
            public string SubentrySameAs { get; set; }
            public byte[] ScriptAsBytes { get; set; }

            /// <summary>
            /// Lists the different subentry labels and their metadata
            /// </summary>
            public SortedDictionary<string, LabelEntry> Labels = new();

            /// <summary>
            /// Key1: Address where the pointer entry is, relative to the start of the string.
            /// This is to prevent duplicate entries.
            /// </summary>
            public SortedDictionary<int, PointerEntry> SubentryPointers = new();
        }

        public sealed class WriteStatus
        {
            public string WriteRegion { get; }
            public long WriteAddress { get; }

            public WriteStatus(string WriteRegion, long WriteAddress)
            {
                this.WriteRegion = WriteRegion;
                this.WriteAddress = WriteAddress;
            }
        }

        public sealed class PointerEntry
        {
            public string GotoSubentryId { get; }
            public int SubentryPointerLocation { get; }
            public EmbeddedPointerFormat EmbeddedPointerFormat { get; }
            public PointerEntry(string gotoSubentryId, int pointerLocation, EmbeddedPointerFormat embeddedPointerFormat)
            {
                this.GotoSubentryId = gotoSubentryId;
                this.SubentryPointerLocation = pointerLocation;
                this.EmbeddedPointerFormat = embeddedPointerFormat;
            }
        }

        public sealed class LabelEntry
        {
            public int LabelLocation { get; }
            public LabelEntry(int labelLocation)
            {
                this.LabelLocation = labelLocation;
            }
        }
    }
}