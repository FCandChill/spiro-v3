﻿using Structs;
using System;
using System.Collections.Generic;
using System.IO;

namespace Spiro.Class
{
    public static class FILE
    {
        private static string CURRENT_ROM_;
        public static string CURRENT_FILE
        {
            get => CURRENT_ROM_;

            set
            {
                if (!ALL.ContainsKey(value))
                {
                    throw new Exception($"File key does not exist: {value}. Make sure it exists in the settings file and that the path is correct.");
                }

                CURRENT_ROM_ = value;
            }
        }
        public static byte[] DATA
        {
            get => ALL[CURRENT_FILE];
            set => ALL[CURRENT_FILE] = value;
        }
        public static Dictionary<string, byte[]> ALL { get; set; }

        public static void ReadFiles()
        {
            ALL = new Dictionary<string, byte[]>();

            foreach (var kvp in Setting.Settings.Files)
            {
                if (File.Exists(Setting.Settings.Files[kvp.Key]))
                {
                    if (!ALL.ContainsKey(kvp.Key))
                    {
                        var b = File.ReadAllBytes(Setting.Settings.Files[kvp.Key]);
                        ALL.Add(kvp.Key, b);
                    }
                    else
                    {
                        throw new Exception($"ROM ALL array already contains the key: {kvp.Key}");
                    }
                }
                else
                {
                    if (Global.Verbose)
                    {
                        Console.WriteLine($"File not found: {Setting.Settings.Files[kvp.Key]}");
                    }
                }
            }
        }
    }
}