﻿namespace Spiro.Class
{
    public class Global
    {
        public static bool Verbose { get; set; }
        public static bool Debug { get; set; }
    }
}