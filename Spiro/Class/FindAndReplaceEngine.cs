﻿using Structs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Spiro.Class
{
    /// <summary>
    /// Transforms text to SquishyText characters.
    /// </summary>
    public class FindAndReplaceEngine
    {
        public string FileName { get; private set; }
        private Enums.FindAndReplace ReplaceMode { get; set; }

        private List<SquishyTextEntry> LSquishyText { get; set; }

        public FindAndReplaceEngine(string FileName, Enums.FindAndReplace replaceMode)
        {
            if (string.IsNullOrEmpty(FileName))
            {
                throw new Exception();
            }
            this.FileName = FileName;
            this.ReplaceMode = replaceMode;
            LSquishyText = new List<SquishyTextEntry>();
        }

        public void ReadSquishyTextFile()
        {
            using StreamReader sr = new StreamReader(FileName);
            string line = "";

            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();

                if (!line.Contains(Constants.DICTIONARY_SPLIT))
                {
                    continue;
                }

                if (line.StartsWith("//"))
                {
                    continue;
                }

                int idx = line.LastIndexOf(Constants.DICTIONARY_SPLIT);
                LSquishyText.Add(new SquishyTextEntry(line[..idx], line[(idx + 1)..]));
            }
        }

        public string AddSquishyText(string dialogueEntry)
        {
            switch (ReplaceMode)
            {
                case Enums.FindAndReplace.SquishyText:
                    return AddSquishyTextNormal(dialogueEntry, false);
                case Enums.FindAndReplace.SquishyTextRegex:
                    return AddSquishyTextNormal(dialogueEntry, true);
                case Enums.FindAndReplace.Regex:
                    return AddSquishyTextRegex(dialogueEntry);
                default:
                    throw new Exception($"Replace mode not supported: {ReplaceMode}");
            }
        }

        public string AddSquishyTextNormal(string DialogueEntry, bool UseRegex)
        {
            string newText = "";

            if (LSquishyText.Count != 0)
            {
                const string PATTERN = @"(\(.*?\))|(\[.*?\])|({.*?})";
                const string EXCLUDE = @"(\(.*?\))|(\[.*?\])|({.*?})";

                string[] a_chunks = Regex.Split(DialogueEntry, PATTERN).Where(s => s != String.Empty).ToArray();

                foreach (string s in a_chunks)
                {
                    string Insert = s;

                    // Don't do find replace actions within brackets, parenthesis, etc.
                    // For example, if we're looking for text like ll, [llama icon] would match.
                    // This is obviously unwanted. As a result, encapsulated string are skipped
                    // over unless regex is enable which can offer protection against this.
                    if (UseRegex || !Regex.IsMatch(s, EXCLUDE))
                    {
                        foreach (SquishyTextEntry entry in LSquishyText)
                        {
                            if (UseRegex)
                            {
                                Insert = Regex.Replace(Insert, entry.Find, entry.Replace);
                            }
                            else
                            {
                                Insert = Insert.Replace(entry.Find, entry.Replace);
                            }
                        }
                    }

                    newText = string.Concat(newText, Insert);
                }
            }
            else
            {
                newText = DialogueEntry;
            }

            return newText;
        }

        public string AddSquishyTextRegex(string DialogueEntry)
        {
            if (LSquishyText.Count == 0)
            {
                return DialogueEntry;
            }

            foreach (SquishyTextEntry entry in LSquishyText)
            {
                DialogueEntry = Regex.Replace(DialogueEntry, entry.Find, entry.Replace);
            }

            return DialogueEntry;
        }
    }

    public struct SquishyTextEntry
    {
        public string Find { private set; get; }
        public string Replace { private set; get; }

        public SquishyTextEntry(string Find, string Replace)
        {
            this.Find = Find;
            this.Replace = Replace;
        }
    }
}