﻿using LunarAddr;
using Newtonsoft.Json;
using Spiro.Class;
using Spiro.Class.PointerManagement;
using Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TextParse;
using static Structs.Enums;
using static Structs.ScriptIndexTable;
using static TextParse.TextParsing;

namespace MainMan
{
    public partial class MainManager
    {
        public void DumpScript()
        {
            List<string> writeToFileId = new List<string>();

            FILE.ReadFiles();

            //read settings file.
            Setting.ReadSettingsFile(settingsFilePath);

            // Key 1: Address
            // Key 2: Entry No
            SortedDictionary<string, SortedDictionary<(long, int), Structs.Entry>> pointerDumpData = new();

            if (Global.Verbose)
            {
                Console.Out.WriteLine($"Getting pointers.");
            }

            foreach (KeyValuePair<string, PointerReadWrite> readKvp in Setting.Settings.Pointers.Read)
            {
                if (readKvp.Value.Disabled)
                {
                    if (Global.Verbose)
                    {
                        Console.Out.WriteLine($"Skipping {readKvp.Key} because disabled.");
                    }

                    continue;
                }

                if (Global.Verbose)
                {
                    Console.Out.WriteLine($"Reading pointers for {readKvp.Key}");
                }

                if (!writeToFileId.Contains(readKvp.Value.RenderFormat.File) && Setting.Settings.Misc.BlankOutTextDataAfterRead)
                {
                    writeToFileId.Add(readKvp.Value.RenderFormat.File);
                }

                FILE.CURRENT_FILE = readKvp.Value.PointerFormat.File;

                if (FILE.DATA == null)
                {
                    throw new Exception($"Having trouble reading file: {Setting.Settings.Files[FILE.CURRENT_FILE]}");
                }

                if (readKvp.Value.PointerFormat.CustomPointerFormat != null && (readKvp.Value.PointerFormat.PointerType == PointerType.Custom ^ readKvp.Value.PointerFormat.CustomPointerFormat.Length != 0))
                {
                    throw new Exception($"{nameof(readKvp.Value.PointerFormat.PointerType)} and {nameof(readKvp.Value.PointerFormat.CustomPointerFormat)} both must be filled out for a custom pointer to be read.");
                }

                pointerDumpData.Add(readKvp.Key, new());

                if (readKvp.Value.PointerFormat.Addresses == null || readKvp.Value.PointerFormat.Addresses.Length == 0)
                {
                    throw new Exception("Addresses cannot be null or empty.");
                }

                foreach (long pointerTableLocation in readKvp.Value.PointerFormat.Addresses)
                {
                    for (int entryNo = 0; entryNo < readKvp.Value.PointerFormat.EntryNumber; entryNo++)
                    {
                        if (pointerDumpData[readKvp.Key].ContainsKey((pointerTableLocation, entryNo)))
                        {
                            throw new Exception($"A text entry with the following information already exists: " +
                               $"{nameof(pointerTableLocation)}: {MyMath.DecToHex(pointerTableLocation, Prefix.X)};" + 
                               $"{nameof(entryNo)}: {entryNo}");
                        }

                        pointerDumpData[readKvp.Key].Add((pointerTableLocation, entryNo), new());

                        var pointerType = readKvp.Value.PointerFormat.PointerType;
                        var pointerGrouping = readKvp.Value.PointerFormat.PointerGrouping;
                        long pointerLength = readKvp.Value.PointerFormat.PointerLength;

                        long pointerAddress = -1;
                        long pointerLocation = -1;
                        long scriptLength = readKvp.Value.RenderFormat.FixedSize;

                        switch (pointerGrouping)
                        {
                            case Enums.PointerGrouping.Group:
                                if (pointerType == Enums.PointerType.None)
                                {
                                    throw new Exception("Can't have the normal pointer grouping when there isn't a pointer.");
                                }

                                pointerLocation = pointerTableLocation + (entryNo * pointerLength) + entryNo * readKvp.Value.PointerFormat.BytesBetween;
                                break;
                            case Enums.PointerGrouping.SingleEntryFixedSize:
                            case Enums.PointerGrouping.Single:
                                pointerLocation = pointerTableLocation;
                                break;
                            default:
                                throw new Exception($"Invalid {nameof(pointerGrouping)} value: {pointerGrouping}");
                        }

                        if (pointerLocation > FILE.ALL[readKvp.Value.PointerFormat.File].Length)
                        {
                            throw new Exception($"Pointer Location out of bounds: " + 
                                $"Region {readKvp.Key}; {nameof(readKvp.Value.PointerFormat.File)}: {readKvp.Value.PointerFormat.File}; " +
                                $"Length: {MyMath.DecToHex(FILE.DATA.Length, Prefix.X)}; " +
                                $"{nameof(pointerTableLocation)}: {MyMath.DecToHex(pointerTableLocation, Prefix.X)}; " +
                                $"{nameof(pointerLocation)}: {MyMath.DecToHex(pointerLocation, Prefix.X)}");
                        }

                        pointerDumpData[readKvp.Key][(pointerTableLocation, entryNo)].AllPointerLocations = pointerLocation;

                        if (pointerLength < 0)
                        {
                            throw new Exception("Cannot have a pointer lenth less than zero.");
                        }

                        byte[] pointerBytes = new byte[pointerLength];

                        if (pointerBytes.Length != 0)
                        {
                            Array.Copy(FILE.DATA, (int)pointerLocation, pointerBytes, 0, pointerBytes.Length);
                        }

                        if (Global.Debug)
                        {
                            Console.WriteLine($"Reading address: {MyMath.DecToHex(pointerLocation, Prefix.X)}; Name: {readKvp.Key};Index: {entryNo:0000};");
                        }

                        bool NoPointer = false;
                        switch (pointerType)
                        {
                            case Enums.PointerType.LittleEndian:
                                PointerManager.ReadLittleEndian(pointerBytes, out pointerAddress);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimaryNoLength:
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimary:
                                PointerManager.GetAddressMetalSladerSNES(pointerBytes, out pointerAddress, out scriptLength);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimaryNoLength:
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimary:
                                PointerManager.GetAddressMetalSladerNES(pointerBytes, out pointerAddress, out scriptLength);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESTransPrimaryNoLength:
                                PointerManager.GetAddressMetalSladerNES(pointerBytes, out pointerAddress, out scriptLength, true);
                                break;
                            case Enums.PointerType.BigEndian:
                                PointerManager.ReadBigEndian(pointerBytes, out pointerAddress);
                                break;
                            case Enums.PointerType.None:
                                NoPointer = true;
                                pointerAddress = pointerTableLocation;
                                break;
                            case Enums.PointerType.Custom:
                                string[] customPointerFormat = readKvp.Value.PointerFormat.CustomPointerFormat;
                                int entryPointerLength = readKvp.Value.PointerFormat.PointerLength;

                                if (customPointerFormat == null)
                                {
                                    throw new Exception($"{nameof(customPointerFormat)} is null");
                                }

                                if (customPointerFormat.Length != entryPointerLength)
                                {
                                    throw new Exception($"{nameof(customPointerFormat)}.Length and {nameof(entryPointerLength)} must be the same: {customPointerFormat.Length} != {entryPointerLength}");
                                }

                                PointerManager.GetAddressCustomFormat(pointerBytes, customPointerFormat, out pointerAddress);
                                break;
                            default:
                                throw new Exception($"Invalid pointertype number: {pointerType}");
                        }

                        switch (pointerGrouping)
                        {
                            case Enums.PointerGrouping.Group:
                                break;
                            case Enums.PointerGrouping.Single:
                                break;
                            case Enums.PointerGrouping.SingleEntryFixedSize:
                                pointerAddress += entryNo * scriptLength;
                                break;
                            default:
                                throw new Exception($"Invalid pointer grouping vakue: {pointerGrouping}");
                        }

                        if (!NoPointer)
                        {
                            // Handle PC difference logic before address conversion logic
                            // This gives the opportunity to adjust the pointer to the correct bank, for example.
                            long pcDiff = readKvp.Value.PointerFormat.PcDifference;

                            if (pointerAddress + pcDiff < 0)
                            {
                                if (!readKvp.Value.PointerFormat.IgnoreOutOfBoundPointers)
                                {
                                    throw new Exception($"Address negative after subtraction. {MyMath.DecToHex(pointerAddress, Prefix.X)} - {MyMath.DecToHex(Math.Abs(pcDiff), Prefix.X)} = -{MyMath.DecToHex(Math.Abs(pointerAddress + pcDiff), Prefix.X)}");
                                }
                            }

                            pointerAddress = pcDiff + pointerAddress;

                            bool isValidPcAddress = true;

                            switch (readKvp.Value.PointerFormat.AddressConversion)
                            {
                                case Enums.AddrConversion.None:
                                    break;
                                case Enums.AddrConversion.LoROM1:
                                    pointerAddress = AddressLoROM1.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                    break;
                                case Enums.AddrConversion.LoROM2:
                                    pointerAddress = AddressLoROM2.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                    break;
                                case Enums.AddrConversion.HiROM:
                                    pointerAddress = AddressHiRom.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                    break;
                                case Enums.AddrConversion.ExLoROM:
                                    pointerAddress = AddressExLoROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                    break;
                                case Enums.AddrConversion.ExHiROM:
                                    pointerAddress = AddressExHiROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                                    break;
                                default:
                                    throw new Exception($"Invalid {nameof(readKvp.Value.PointerFormat.AddressConversion)}: {readKvp.Value.PointerFormat.AddressConversion}");
                            }

                            if (!isValidPcAddress)
                            {
                                throw new Exception("Address conversion failed.");
                            }

                            if (!FILE.ALL.ContainsKey(readKvp.Value.RenderFormat.File))
                            {
                                throw new KeyNotFoundException($"The following key from \"{readKvp.Key}\"'s {nameof(readKvp.Value.RenderFormat)} does not exist in Files: {readKvp.Value.RenderFormat.File}");
                            }

                            if ((int)pointerAddress > FILE.ALL[readKvp.Value.RenderFormat.File].Length)
                            {
                                throw new IndexOutOfRangeException($"Pointer bigger than ROM size. Key: {readKvp.Key}; {nameof(entryNo)}: {entryNo}; " +
                                    $"{nameof(pointerTableLocation)}: {MyMath.DecToHex(pointerTableLocation, Prefix.X)}; {nameof(pointerAddress)}: {MyMath.DecToHex(pointerAddress)}");
                            }
                        }

                        if (Global.Debug)
                        {
                            Console.WriteLine($"Bytes: {MyMath.FormatByteInBrackets(pointerBytes)}; {nameof(pointerAddress)}: 0x{pointerAddress:X8}; {nameof(scriptLength)}: 0x{scriptLength:X3}");
                        }

                        // AddressOfPointer, EntryNo
                        pointerDumpData[readKvp.Key][(pointerTableLocation, entryNo)].AllTextLocation = pointerAddress;
                        pointerDumpData[readKvp.Key][(pointerTableLocation, entryNo)].AllLength = scriptLength;
                    }
                }
            }

            File.Delete(Setting.Settings.Scripts.ScriptPath);

            ScriptIndexTable dumpedScript = null;
            TextParsing txtIO;
            for (; DmpData.PassNumber < 2; DmpData.PassNumber++)
            {
                dumpedScript = new();
                txtIO = new();

                if (Global.Verbose)
                {
                    Console.Out.WriteLine($"Reading data");
                }

                Stopwatch timer = Stopwatch.StartNew();

                foreach (KeyValuePair<string, PointerReadWrite> readKvp in Setting.Settings.Pointers.Read)
                {
                    foreach (var a in readKvp.Value.RenderFormat.Tables)
                    {
                        txtIO.LoadNewTableFile
                        (
                            readOriginal: true,
                            te: a.Value,
                            mainTe: readKvp.Value.RenderFormat.TableMain
                        );
                    }

                    txtIO.QuickChange(true, readKvp.Value.RenderFormat.TableMain.Value);

                    dumpedScript.IndexTable.Add(readKvp.Key, new ScriptIndexTable.Entry());

                    if (readKvp.Value.Disabled)
                    {
                        if (Global.Verbose)
                        {
                            Console.Out.WriteLine($"Skipping {readKvp.Key} because disabled.");
                        }

                        continue;
                    }

                    if (Global.Verbose)
                    {
                        Console.Out.WriteLine($"Getting data for: {readKvp.Key}");
                    }

                    if (readKvp.Value.RenderFormat.IncludeRedirects && Global.Verbose && readKvp.Value.RenderFormat.DialogueReadType == Enums.DialogueReadType.HasLengthVar)
                    {
                        Console.WriteLine($"WARNING: {nameof(readKvp.Value.RenderFormat.IncludeRedirects)} enabled for " +
                            $"{nameof(Enums.DialogueReadType.HasLengthVar)} {nameof(readKvp.Value.RenderFormat.DialogueReadType)}. This will produce false positives.");
                    }

                    foreach (var entryKvp in pointerDumpData[readKvp.Key])
                    {
                        var pointerGrouping = readKvp.Value.PointerFormat.PointerGrouping;
                        int entryNo = entryKvp.Key.Item2;

                        Dictionary<int, ReadByteStreamReturn> rbr = null;

                        if (Global.Debug)
                        {
                            Console.Out.WriteLine($"Index table: {readKvp.Key}; PointerTable:{MyMath.DecToHex(entryKvp.Key.Item1, Prefix.X)}; EntryNo: {entryNo}");
                        }

                        if (!dumpedScript.IndexTable[readKvp.Key].PointerTable.ContainsKey(entryKvp.Key.Item1))
                        {
                            dumpedScript.IndexTable[readKvp.Key].PointerTable.Add(entryKvp.Key.Item1, new ScriptIndexTable.PointerTable());
                        }

                        FILE.CURRENT_FILE = readKvp.Value.RenderFormat.File;

                        switch (readKvp.Value.RenderFormat.DialogueReadType)
                        {
                            case Enums.DialogueReadType.Normal:
                            case Enums.DialogueReadType.HasDelimiter:
                                // Originally "DialogueReadType.HasDelimiter" and "RenderType.Dictionary" had their own functions.
                                // However, since the logic was extremely similar, they were merged into one function.

                                var useDelimeter = readKvp.Value.RenderFormat.Delimiters != null && readKvp.Value.RenderFormat.Delimiters.Length != 0;
                                rbr = txtIO.ReadByteStreamWithPointer(new TextParsing.ReadByteStreamArgs(
                                    EntryNo: entryNo,
                                    StartTextAddress: entryKvp.Value.AllTextLocation,
                                    Key: readKvp.Key,
                                    PointerLocation: entryKvp.Value.AllPointerLocations,
                                    Prw: readKvp.Value,
                                    Buffer: FILE.DATA,
                                    IncludeRedirects: readKvp.Value.RenderFormat.IncludeRedirects,
                                    UsesDelimiter: useDelimeter
                                    )
                                );
                                break;
                            case Enums.DialogueReadType.HasLengthVar:
                                rbr = txtIO.ReadByteStreamWithPointer(new ReadByteStreamArgs(
                                    entryNo,
                                    entryKvp.Value.AllTextLocation,
                                    readKvp.Key,
                                    entryKvp.Value.AllPointerLocations,
                                    readKvp.Value,
                                    FILE.DATA,
                                    readKvp.Value.RenderFormat.IncludeRedirects,
                                    UsesDelimiter: false,
                                    ByteSize: pointerDumpData[readKvp.Key][(entryKvp.Key.Item1, entryNo)].AllLength)
                                    );
                                break;
                            case Enums.DialogueReadType.MarioPicrossSNES:
                                rbr = txtIO.ReadByteMarioPicrossSNES(entryNo,
                                    entryKvp.Value.AllTextLocation,
                                    readKvp.Key,
                                    readKvp.Value,
                                    entryKvp.Value.AllPointerLocations);
                                break;
                            case Enums.DialogueReadType.TensaiBakabon:
                                rbr = txtIO.ReadByteTensaiBakabon(
                                    new TextParsing.ReadByteStreamArgs(entryNo,
                                    entryKvp.Value.AllTextLocation,
                                    readKvp.Key,
                                    entryKvp.Value.AllPointerLocations,
                                    readKvp.Value,
                                    FILE.DATA,
                                    readKvp.Value.RenderFormat.IncludeRedirects,
                                    true));
                                break;
                            // Pascal strings
                            case Enums.DialogueReadType.LengthAtBeginningOfLine:
                                rbr = txtIO.ReadByteStreamWithPointerInSizeAtBeginning(new ReadByteStreamArgs(
                                    entryNo,
                                    entryKvp.Value.AllTextLocation,
                                    readKvp.Key,
                                    entryKvp.Value.AllPointerLocations,
                                    readKvp.Value,
                                    FILE.DATA,
                                    readKvp.Value.RenderFormat.IncludeRedirects,
                                    UsesDelimiter: false), 1, addOne: true);
                                break;
                            default:
                                throw new Exception();
                        }

                        //bool hasPointer = true;
                        bool cascadingEntries = false;

                        switch (pointerGrouping)
                        {
                            case Enums.PointerGrouping.Group:
                                break;
                            case Enums.PointerGrouping.SingleEntryFixedSize:
                            case Enums.PointerGrouping.Single:
                                cascadingEntries = true;
                                break;
                            default:
                                throw new Exception($"Invalid {nameof(pointerGrouping)} value: {pointerGrouping}");
                        }

                        if (DmpData.PassNumber == 0)
                        {
                            // If each entry is one after the other, adjust the next entry's pointer accordingly.
                            if (cascadingEntries && pointerDumpData.ContainsKey(readKvp.Key) && pointerDumpData[readKvp.Key].ContainsKey((entryKvp.Key.Item1, entryNo)))
                            {
                                var nextKey = (entryKvp.Key.Item1, entryNo + 1);
                                if (pointerDumpData[readKvp.Key].ContainsKey(nextKey))
                                {
                                    int scriptTotalByteLength = (from x in rbr.ToList() select x.Value.ScriptBytes.Count).Sum();
                                    pointerDumpData[readKvp.Key][nextKey].AllTextLocation = entryKvp.Value.AllTextLocation + scriptTotalByteLength;
                                }
                            }
                        }

                        if (Global.Debug)
                        {
                            Console.Out.WriteLine($"Primary: { MyMath.DecToHex(pointerDumpData[readKvp.Key][(entryKvp.Key.Item1, entryNo)].AllTextLocation, Prefix.X)}:");
                        }

                        if (Global.Debug)
                        {
                            foreach (var ss in rbr)
                            {
                                Console.Out.WriteLine(MyMath.FormatBytesInBrackets(ss.Value.ScriptBytes.ToArray()));
                            }
                        }

                        //Load dictionary if it's the first entry. Modify text file.
                        if (readKvp.Value.RenderFormat.IsDictionary)
                        {
                            foreach (var ss in rbr)
                            {
                                txtIO.AddValueToDictionary(ss.Value.ScriptBytesWithoutDelimiter.ToArray());
                            }
                        }

                        for (int rbrI = 0; rbrI < rbr.Count; rbrI++)
                        {
                            switch (readKvp.Value.RenderFormat.RenderType)
                            {
                                //Byte => text with dictionary table.
                                case RenderType.LengthAtBeginningOfLine:
                                case RenderType.TensaiBakabon:
                                case RenderType.Dictionary:
                                    break;
                                case RenderType.NoDictionaryMetalSladerNameCard:
                                    rbr[rbrI].ScriptString = txtIO.BytesToStringMetalSladerNameCards(
                                        rbr[rbrI].ScriptBytes.ToArray(),
                                        entryKvp.Value.AllTextLocation,
                                        readKvp.Key,
                                        entryKvp.Value.AllPointerLocations,
                                        entryNo,
                                        readKvp.Value);
                                    break;
                                case RenderType.KaettekitaMarioBrothersNagatanienWorldIntermission:
                                    rbr[rbrI].ScriptString = txtIO.BytesToStringKaettekitaMarioBrothers(
                                        rbr[rbrI].ScriptBytes,
                                        entryKvp.Value.AllTextLocation,
                                        entryNo,
                                        readKvp.Key,
                                        entryKvp.Value.AllPointerLocations,
                                        readKvp.Value
                                        );
                                    break;
                                default:
                                    throw new Exception($"Invalid {nameof(readKvp.Value.RenderFormat.RenderType)}: {readKvp.Value.RenderFormat.RenderType}");
                            }
                        }

                        if (!string.IsNullOrEmpty(readKvp.Value.RenderFormat.FindAndReplaceFile))
                        {
                            FindAndReplaceEngine squish = new(string.Format(Constants.Path_To_x_orig, readKvp.Value.RenderFormat.FindAndReplaceFile), readKvp.Value.RenderFormat.FindAndReplaceStrategy);
                            squish.ReadSquishyTextFile();
                            foreach (var ss in rbr)
                            {
                                ss.Value.ScriptString = squish.AddSquishyText(ss.Value.ScriptString);
                            }
                        }

                        long pointerLocation = pointerDumpData[readKvp.Key][(entryKvp.Key.Item1, entryNo)].AllPointerLocations;
                        long textLocation = pointerDumpData[readKvp.Key][(entryKvp.Key.Item1, entryNo)].AllTextLocation;

                        dumpedScript.IndexTable[readKvp.Key].PointerTable[entryKvp.Key.Item1].PointerTableEntry.Add(entryKvp.Key.Item2, new ScriptIndexTable.PointerTableEntry());

                        var currentPointerTable = dumpedScript.IndexTable[readKvp.Key].PointerTable[entryKvp.Key.Item1].PointerTableEntry[entryKvp.Key.Item2];

                        if (DmpData.PassNumber == 1)
                        {
                            int depth = 0;
                            foreach (var kvpRet in rbr)
                            {
                                if (kvpRet.Value.Id == kvpRet.Value.SameAsId)
                                {
                                    throw new($"Id cannot be the same as {nameof(kvpRet.Value.SameAsId)} field. Id: {kvpRet.Value.Id}; {nameof(pointerLocation)}: {MyMath.DecToHex(pointerLocation, Prefix.X)}");
                                }

                                DmpData.SubentrMetadata[kvpRet.Value.Id].DumpNumber++;

                                // If the label was already dumped, just have a tag for the first subentry
                                // as the other subentries will also already be dumped before.
                                // However, don't print the entry, just reference the original dump.
                                if (DmpData.SubentrMetadata[kvpRet.Value.Id].DumpNumber > 1)
                                {
                                    if (depth == 0)
                                    {
                                        DmpData.SubentrMetadata[kvpRet.Value.Id].DumpNumber++;
                                        kvpRet.Value.SameAsId = kvpRet.Value.Id;
                                        kvpRet.Value.Id += $"_{DmpData.SubentrMetadata[kvpRet.Value.Id].DumpNumber}";
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }

                                // If the subentry just references a label, omit it.
                                // The only exception is the first subentry.
                                // In which case, modify the sameas to be unique and then point sameas to the label.
                                // This is so when during the writing process, the pointer table is updated to 
                                // point to the label.
                                if (kvpRet.Value.IsSubentryLabel)
                                {
                                    kvpRet.Value.SameAsId = kvpRet.Value.Id;
                                    kvpRet.Value.Id += "G";
                                }

                                SubEntries e = new();
                                if (Setting.Settings.Misc.IncludedPointerMetadata)
                                {
                                    var pointerFile = Setting.Settings.Pointers.Read[readKvp.Key].PointerFormat.File;
                                    var textFile = Setting.Settings.Pointers.Read[readKvp.Key].RenderFormat.File;

                                    e.Metadata = new(
                                        PointerFile: pointerFile,
                                        TextFile: textFile,
                                        PointerLocation: pointerLocation,
                                        TextLocation: kvpRet.Value.TextLocation,
                                        ByteLength: kvpRet.Value.ScriptBytes.Count
                                    );
                                }

                                if (kvpRet.Value.SameAsId != null)
                                {
                                    if (depth != 0)
                                    {
                                        continue;
                                    }

                                    e.SamePointerAs = kvpRet.Value.SameAsId;

                                    // The byte reading logic ends the reading early, so we didn't get the chance to get the byte length,
                                    // so obtain it from the entry it references.
                                    if (Setting.Settings.Misc.IncludedPointerMetadata)
                                    {
                                        e.Metadata.ByteLength = kvpRet.Value.ScriptBytes.Count;
                                    }
                                }
                                else
                                {
                                    if (Setting.Settings.Scripts.Original)
                                    {
                                        e.Text.Add(Enums.ScriptTypes.original, kvpRet.Value.ScriptString ?? null);
                                    }

                                    if (Setting.Settings.Scripts.New)
                                    {
                                        if (Setting.Settings.Misc.MirrorOriginalToNewOnDump)
                                        {
                                            e.Text.Add(Enums.ScriptTypes.@new, kvpRet.Value.ScriptString ?? null);
                                        }
                                        else
                                        {
                                            e.Text.Add(Enums.ScriptTypes.@new, kvpRet.Value.ScriptString == null ? null : "");
                                        }
                                    }

                                    if (Setting.Settings.Scripts.Comment)
                                    {
                                        e.Text.Add(Enums.ScriptTypes.comment, kvpRet.Value.ScriptString == null ? null : "");
                                    }

                                    if (Setting.Settings.Scripts.Menu)
                                    {
                                        e.Text.Add(Enums.ScriptTypes.menu, kvpRet.Value.ScriptString == null ? null : "");
                                    }

                                    if (Setting.Settings.Scripts.Bytes)
                                    {
                                        e.Text.Add(Enums.ScriptTypes.bytes, kvpRet.Value.ScriptString == null ? null : MyMath.FormatBytesInBrackets(kvpRet.Value.ScriptBytes.ToArray()));
                                    }

                                    if (Setting.Settings.Scripts.Proof)
                                    {
                                        e.Text.Add(Enums.ScriptTypes.proof, kvpRet.Value.ScriptString == null ? null : "");
                                    }
                                }

                                // Not sure if there's any way to omit empty dictionaries other than setting them to null.
                                if (e.Text.Count == 0)
                                {
                                    e.Text = null;
                                }

                                if (!currentPointerTable.Subentries.ContainsKey(kvpRet.Value.Id))
                                {
                                    currentPointerTable.Subentries.Add(kvpRet.Value.Id, e);
                                }
                                else
                                {
                                    throw new($"Subentry with the id {kvpRet.Value.Id} was already dumped.");
                                }

                                depth++;
                            }
                        }
                    }

                    if (readKvp.Value.RenderFormat.IsDictionary)
                    {
                        txtIO.LoadNewTableFile
                        (
                            readOriginal: true,
                            te: readKvp.Value.RenderFormat.TableMain.Value,
                            mainTe: readKvp.Value.RenderFormat.TableMain
                        );
                    }
                }

                timer.Stop();

                if (Global.Verbose)
                {
                    Console.WriteLine($"Dumped script text in: {TimeSpan.FromMilliseconds(timer.ElapsedMilliseconds).TotalSeconds} second(s).");
                }
            }

            if (!Directory.Exists(Path.GetDirectoryName(Setting.Settings.Scripts.ScriptPath)))
            {
                throw new($"Path for script doesn't exist: {Path.GetFullPath(Path.GetDirectoryName(Setting.Settings.Scripts.ScriptPath))}");
            }

            var camelSettings = new JsonSerializerSettings { ContractResolver = new CamelCaseExceptDictionaryKeysResolver() };
            File.WriteAllText(Setting.Settings.Scripts.ScriptPath, JsonConvert.SerializeObject(dumpedScript, Formatting.Indented, camelSettings));

            foreach (string ss in writeToFileId)
            {
                /*
                 * The "BlankOutTextDataAfterRead" is used to figure out what regions of the rom store a script.
                 * The rom is modified to do so, however, we don't want to modify the original roms, so ".blanked"
                 * is added to the extension.
                 */
                File.WriteAllBytes(Path.ChangeExtension(Setting.Settings.Files[ss], Path.GetExtension(Setting.Settings.Files[ss]) + ".blanked"), FILE.ALL[ss]);
            }
        }
    }
}