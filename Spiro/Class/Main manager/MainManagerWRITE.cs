﻿using LunarAddr;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Spiro.Class;
using Spiro.Class.PointerManagement;
using Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TextParse;
using static Structs.Enums;

namespace MainMan
{
    public partial class MainManager
    {
        private readonly string settingsFilePath = "Settings.json";

        public MainManager(string projectFolder)
        {
            Directory.SetCurrentDirectory(projectFolder);
            Setting.ReadSettingsFile(settingsFilePath);
        }

        public void WriteScriptToROM()
        {
            FILE.ReadFiles();

            //Load in the CHR table, until we find the dictionary and load it in.
            TextParsing txtIO = new();


            //read settings file.
            Setting.ReadSettingsFile(settingsFilePath);

            // Read script
            var options = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };
            var Json = JsonConvert.DeserializeObject<ScriptIndexTable>(new StreamReader(Setting.Settings.Scripts.ScriptPath).ReadToEnd(), options);

            if (Global.Verbose)
            {
                Console.WriteLine($"Converting script to binary data.");
            }

            var timer = Stopwatch.StartNew();

            WidthCalc lengthCalc = new(Setting.Settings.Misc.LineBreakChar);

            int byteSize = 0;
            int characterSize = 0;
            int bytesLost = 0;
            string currentChr = "";
            Dictionary<string, FindAndReplaceEngine> squish = new();

            // Populate metadata
            // as well as validate the script
            foreach (var indexTableEntry in Json.IndexTable)
            {
                if (!Setting.Settings.Pointers.Write.ContainsKey(indexTableEntry.Key))
                {
                    throw new KeyNotFoundException($"Key doesn't exist in settings file but is referenced in script: {indexTableEntry.Key}");
                }

                var renderFormat = Setting.Settings.Pointers.Write[indexTableEntry.Key].RenderFormat;
                if (!string.IsNullOrEmpty(renderFormat.FindAndReplaceFile) && !squish.ContainsKey(renderFormat.FindAndReplaceFile))
                {
                    squish.Add(renderFormat.FindAndReplaceFile, new(
                        string.Format(Constants.Path_To_x_new, renderFormat.FindAndReplaceFile),
                        renderFormat.FindAndReplaceStrategy));

                    squish[renderFormat.FindAndReplaceFile].ReadSquishyTextFile();
                }

                WriteData.IndexTableInfo.Add(indexTableEntry.Key, new());

                foreach (var PointerTableEntry in indexTableEntry.Value.PointerTable)
                {
                    foreach (var pointerEntry in PointerTableEntry.Value.PointerTableEntry)
                    {
                        int entryNo = pointerEntry.Key;
                        if (entryNo >= Setting.Settings.Pointers.Write[indexTableEntry.Key].PointerFormat.EntryNumber)
                        {
                            throw new IndexOutOfRangeException($"EntryNo {entryNo} out of range. " + 
                                $"Must be less than {Setting.Settings.Pointers.Write[indexTableEntry.Key].PointerFormat.EntryNumber}. " + 
                                $"Entry owner: {indexTableEntry.Key}; " + 
                                $"Address: {MyMath.DecToHex(PointerTableEntry.Key, Prefix.X)}");
                        }

                        int subentryIndex = 0;
                        foreach (var subEntry in pointerEntry.Value.Subentries)
                        {
                            var pointerTableLocation = PointerTableEntry.Key;

                            var subentryId = subEntry.Key;

                            if (subentryId == "intro_000015")
                            {

                            }

                            if (subentryId == "intro_000016")
                            {

                            }

                            WriteData.SubentrMetadata.Add(subentryId, new());
                            WriteData.SubentrMetadata[subentryId].Id = subentryId;
                            WriteData.SubentrMetadata[subentryId].SubentryIndex = subentryIndex++;
                            WriteData.SubentrMetadata[subentryId].IndexTable = indexTableEntry.Key;
                            WriteData.SubentrMetadata[subentryId].PointerTableLocation = pointerTableLocation;
                            WriteData.SubentrMetadata[subentryId].EntryNo = pointerEntry.Key;
                        }
                    }
                }
            }

            for (int passNo = 0; passNo < 2; passNo++)
            {
                Console.WriteLine($"Pass number: #{passNo + 1}");

                // Key1: Name of indextable
                foreach (var sciptEntry in Json.IndexTable)
                {
                    Console.WriteLine($"Encoding indextable: {sciptEntry.Key}");

                    //CHR & Dictionary management start
                    string newChrFile = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.Tables["main"].ChrFile;
                    string newDictionary = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.Tables["main"].DictionaryFile;
                    int pixelsPerLine = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.PixelsPerLine;
                    bool isDictionary = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.IsDictionary;

                    if (!string.IsNullOrEmpty(newChrFile))
                    {
                        //Load CHR data into length_calc
                        lengthCalc.LoadCHR(newChrFile, newDictionary);

                        //If the CHR file changes.
                        if (newChrFile != currentChr)
                        {
                            //Update the file path for the CHR translation
                            currentChr = newChrFile;
                        }
                    }

                    foreach (var a in Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.Tables)
                    {
                        txtIO.LoadNewTableFile
                        (
                            readOriginal: false,
                            te: a.Value,
                            mainTe: Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.TableMain
                        );
                    }

                    txtIO.QuickChange(false, Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.TableMain.Value);

                    if (isDictionary)
                    {
                        lengthCalc.LoadDictionaryTemplate(newChrFile, newDictionary);
                    }

                    // Key1: Address of pointer table
                    foreach (var entry in sciptEntry.Value.PointerTable)
                    {
                        // Key1: Line index
                        foreach (var pointerTableEntry in entry.Value.PointerTableEntry)
                        {
                            if (Global.Debug)
                            {
                                Console.Out.WriteLine($"Pointer Table: {MyMath.DecToHex(entry.Key, Prefix.X)}; Line number: {pointerTableEntry.Key}; ");
                            }

                            int entryNo = pointerTableEntry.Key;

                            foreach (var subentry in pointerTableEntry.Value.Subentries)
                            {
                                if (Global.Debug)
                                {
                                    Console.Out.WriteLine($"Subentry id: {subentry.Key}");
                                }

                                // See if a line points to the same line as another entry.
                                var sameAsId = subentry.Value.SamePointerAs;

                                if (!(string.IsNullOrEmpty(sameAsId) ^ (subentry.Value.Text.Count == 0 || (!subentry.Value.Text.ContainsKey(ScriptTypes.@new) || subentry.Value.Text[ScriptTypes.@new] == null))))
                                {
                                    throw new Exception($"Either the {nameof(sameAsId)} field or new text field must be filled out.");
                                }

                                var subentryInfo = WriteData.SubentrMetadata[subentry.Key];
                                subentryInfo.SubentrySameAs = sameAsId;
                                subentryInfo.ScriptAsBytes = Array.Empty<byte>();

                                if (!string.IsNullOrEmpty(sameAsId))
                                {
                                    // if the sameas is referencing the start of a subentry, just assign 
                                    // it to the value if it's ScriptAsBytes array
                                    if (WriteData.SubentrMetadata.ContainsKey(sameAsId))
                                    {
                                        subentryInfo.ScriptAsBytes = WriteData.SubentrMetadata[sameAsId].ScriptAsBytes;
                                    }
                                    // If the sameAs is referencing in the middle of a subenty, take a subset of the line.
                                    else if (WriteData.SubentrLabelMetadata.ContainsKey(sameAsId))
                                    {
                                        var subEnt = WriteData.SubentrLabelMetadata[sameAsId];
                                        var location = subEnt.Labels[sameAsId].LabelLocation;

                                        var subentrySize = subEnt.ScriptAsBytes.Length - location;
                                        var bytes = new byte[subentrySize];

                                        Array.Copy(subEnt.ScriptAsBytes.ToArray(), location, bytes, 0, subentrySize);

                                        subentryInfo.ScriptAsBytes = bytes;
                                    }
                                    else
                                    {
                                        if (passNo == 1)
                                        {
                                            throw new Exception($"Label is invalid: {sameAsId}.");
                                        }
                                    }
                                }
                                else
                                {
                                    string scriptAsText = subentry.Value.Text[ScriptTypes.@new];
                                    var squishTextFile = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.FindAndReplaceFile;

                                    if (!string.IsNullOrEmpty(squishTextFile) && squish.ContainsKey(squishTextFile))
                                    {
                                        scriptAsText = squish[squishTextFile].AddSquishyText(scriptAsText);
                                    }

                                    switch (Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak)
                                    {
                                        case Enums.AutoLineBreak.None:
                                            break;
                                        case Enums.AutoLineBreak.MetalSladerGlory:
                                            scriptAsText = lengthCalc.MetalSladerTransAddLineBreaks(subentry.Key, scriptAsText, pixelsPerLine, txtIO.Chara);
                                            break;
                                        case Enums.AutoLineBreak.TokimekiMiho:
                                            scriptAsText = lengthCalc.TokimekiMihoTransAddLineBreaks(subentry.Key, scriptAsText, pixelsPerLine, txtIO.Chara, true);
                                            break;
                                        case Enums.AutoLineBreak.TokimekiMihoMessage:
                                            scriptAsText = lengthCalc.TokimekiMihoTransAddLineBreaks(subentry.Key, scriptAsText, pixelsPerLine, txtIO.Chara, false);
                                            break;
                                        default:
                                            throw new Exception($"Autoline break. Invalid id: {Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.AutoLineBreak}");
                                    }

                                    if (!string.IsNullOrEmpty(squishTextFile) && squish.ContainsKey(squishTextFile))
                                    {
                                        scriptAsText = squish[squishTextFile].AddSquishyText(scriptAsText);
                                    }

                                    if (subentry.Key == "dialogue1_carved_000072")
                                    {

                                    }

                                    var renderType = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.RenderType;

                                    switch (renderType)
                                    {
                                        case RenderType.Dictionary:
                                        case RenderType.NoDictionaryMetalSladerNameCard:
                                        case RenderType.KaettekitaMarioBrothersNagatanienWorldIntermission:
                                            subentryInfo.ScriptAsBytes = txtIO.ConvertScriptToHexWithDictionary(scriptAsText, subentry.Key, Setting.Settings.Pointers.Write[sciptEntry.Key], entry.Key).ToArray();
                                            break;
                                        case RenderType.MarioPicrossSNES:
                                            subentryInfo.ScriptAsBytes = txtIO.ConvertScriptToHexMarioPicrossSNES(scriptAsText, subentry.Key, Setting.Settings.Pointers.Write[sciptEntry.Key], entry.Key).ToArray();
                                            break;
                                        case RenderType.TensaiBakabon:
                                            subentryInfo.ScriptAsBytes = txtIO.ConvertScriptToHexTensaiBakabon(scriptAsText, subentry.Key, Setting.Settings.Pointers.Write[sciptEntry.Key], entry.Key);
                                            break;
                                        case RenderType.LengthAtBeginningOfLine:
                                            subentryInfo.ScriptAsBytes = txtIO.ConvertScriptToHexLengthAtBeginning(
                                                scriptAsText,
                                                subentry.Key,
                                                Setting.Settings.Pointers.Write[sciptEntry.Key],
                                                entry.Key,
                                                Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.LengthAtBeginningOfLineDifference
                                                );
                                            break;
                                        default:
                                            throw new Exception($"Invalid {nameof(renderType)}: {renderType}");
                                    }

                                    if (Setting.Settings.Pointers.Write[sciptEntry.Key].PointerFormat.PointerGrouping == Enums.PointerGrouping.SingleEntryFixedSize)
                                    {
                                        var fixedSize = Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.FixedSize;

                                        if (fixedSize != subentryInfo.ScriptAsBytes.Length)
                                        {
                                            throw new Exception($"Fixed size invalid: {fixedSize} != {subentryInfo.ScriptAsBytes.Length}");
                                        }
                                    }

                                    byteSize += subentryInfo.ScriptAsBytes.Length;
                                    characterSize += subentry.Value.Text[Enums.ScriptTypes.@new].Length;
                                }

                                if (!WriteData.IndexTableInfo[sciptEntry.Key].ContainsKey(subentry.Key))
                                {
                                    WriteData.IndexTableInfo[sciptEntry.Key].Add(subentry.Key, subentryInfo);
                                }
                                else
                                {
                                    WriteData.IndexTableInfo[sciptEntry.Key][subentry.Key] = subentryInfo;
                                }

                                if (passNo == 0)
                                {
                                    if (isDictionary)
                                    {
                                        var dictionaryEntry = subentry.Value.Text[Enums.ScriptTypes.@new];
                                        var dictionaryBytes = subentryInfo.ScriptAsBytes.ToArray();

                                        // Remove delimiter
                                        foreach (string s in Setting.Settings.Pointers.Write[sciptEntry.Key].RenderFormat.Delimiters)
                                        {
                                            if (dictionaryEntry.EndsWith(s))
                                            {
                                                var indexOfDelimiter = dictionaryEntry.LastIndexOf(s);
                                                dictionaryEntry = dictionaryEntry[..indexOfDelimiter];

                                                if (!(
                                                    txtIO.Chara.GetCHRByteValue(s, out byte[] delimiterBytes) || 
                                                    txtIO.Chara.GetDictionaryByteValue(s, out delimiterBytes)
                                                   ))
                                                {
                                                    throw new Exception($"Adding string {dictionaryEntry} to dictionary failed. \"{s}\" is an invalid delimiter.");
                                                }

                                                dictionaryBytes = dictionaryBytes.Take(dictionaryBytes.Length - 1).ToArray();
                                                break;
                                            }
                                        }

                                        lengthCalc.AddDictionaryEntryLength(dictionaryEntry, txtIO.Chara);
                                        txtIO.AddValueToDictionary(dictionaryBytes);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            timer.Stop();

            if (Global.Verbose)
            {
                Console.WriteLine($"Converted script to bytes in: {TimeSpan.FromMilliseconds(timer.ElapsedMilliseconds).TotalSeconds} second(s).");
                Console.WriteLine($"Compressed script size: {MyMath.DecToHex(byteSize, Prefix.X)} bytes.");
                Console.WriteLine($"Original script size: {characterSize} characters.");
            }

            /*
             * We just finished converting the script to byte data. Now let's prepare to
             * write to the ROM.
             */

            foreach (string k in Setting.Settings.Write.WriteableRange.Keys)
            {
                WriteData.WriteRegionSpaceUsed.Add(k, 0);
            }

            if (Global.Verbose)
            {
                Console.WriteLine($"Mapping out where to write parsed script data.");
            }

            timer = Stopwatch.StartNew();

            // Blank out each writable region
            // Makes debugging easier
            foreach (var WriteableRangeEntry in Setting.Settings.Write.WriteableRange.Values)
            {
                FILE.CURRENT_FILE = WriteableRangeEntry.File;
                // https://stackoverflow.com/questions/345047/declaring-variables-within-for-loops
                byte[] bytes = Enumerable.Repeat(Setting.Settings.Misc.BlankOutByte, (int)WriteableRangeEntry.Size).ToArray();
                Array.Copy(bytes, 0, FILE.DATA, WriteableRangeEntry.StartAddress, bytes.Length);
            }

            foreach (var writeRegionEntry in Setting.Settings.Write.WriteRegion)
            {
                int writeRegionI = 0;

                foreach (string entryOwner in writeRegionEntry.EntryOwners)
                {
                    if (!Setting.Settings.Pointers.Write.ContainsKey(entryOwner))
                    {
                        throw new Exception($"Invalid WriteRegion {nameof(entryOwner)}: {entryOwner}");
                    }

                    var linesSorted = new Stack<KeyValuePair<string, WriteData.WriteSubentryMetadata>>();
                    var placementStrategy = Setting.Settings.Pointers.Write[entryOwner].PointerFormat.PlacementStrategy;

                    // Setup LinesSortedBySize
                    {
                        if (!WriteData.IndexTableInfo.ContainsKey(entryOwner))
                        {
                            throw new Exception($"The translated script doesn't contain the entry owner \"{entryOwner}\"");
                        }

                        Console.WriteLine($"Writing entry owner: {entryOwner}.");

                        switch (placementStrategy)
                        {
                            // Insert from biggest to small
                            case Enums.PlacementStrategy.Bestfit:
                                if (Global.Verbose)
                                {
                                    Console.WriteLine("Space saving mode activated.");
                                    Console.WriteLine("Lines will be written out of order from biggest to smallest.");
                                }

                                var linesbysize = new SortedList<int, KeyValuePair<string, WriteData.WriteSubentryMetadata>>(new DuplicateKeyComparer<int>());

                                foreach (var e in WriteData.IndexTableInfo[entryOwner])
                                {
                                    linesbysize.Add(e.Value.ScriptAsBytes.Length, new(e.Key, e.Value));
                                }

                                foreach (var kvp in linesbysize)
                                {
                                    linesSorted.Push(kvp.Value);
                                }
                                break;
                            case Enums.PlacementStrategy.InOrder:
                                // store entries sequentially.
                                var temp = WriteData.IndexTableInfo[entryOwner].ToList();
                                temp.Reverse();

                                foreach (var e in temp)
                                {
                                    linesSorted.Push(new(e.Key, e.Value));
                                }
                                break;
                            default:
                                throw new Exception($"Invalid {nameof(Enums.PlacementStrategy)}: {placementStrategy}");

                        }
                    }

                    while (linesSorted.Count != 0)
                    {
                        var kvp = linesSorted.Pop();

                        var subentryId = kvp.Key;

                        long pointerTableLocation = WriteData.SubentrMetadata[subentryId].PointerTableLocation;
                        var lineId = WriteData.SubentrMetadata[subentryId].EntryNo;
                        var scriptBytesToInsert = kvp.Value.ScriptAsBytes;

                        //////////////////////////////////////////
                        /// SameAs logic
                        //////////////////////////////////////////

                        // Determine if we already inserted the data before.
                        if (WriteData.SubentryWriteLocationsInfo.ContainsKey(subentryId))
                        {
                            continue;
                        }

                        // Update the script bytes according to the sameAs field before we start determining where
                        // to write the data
                        // Doing this before calculating the pointers is a bit clumsy but it's necessary.
                        var sameAsId = WriteData.SubentrMetadata[subentryId].SubentrySameAs;
                        if (!string.IsNullOrEmpty(sameAsId))
                        {
                            // Determine if it's a label
                            if (WriteData.SubentrLabelMetadata.ContainsKey(sameAsId))
                            {
                                // Determine if the parent was already written.
                                // If it hasn't, we will write the data very soon.
                                var parentSubentryId = WriteData.SubentrLabelMetadata[sameAsId].Id;
                                if (WriteData.SubentryWriteLocationsInfo.ContainsKey(parentSubentryId))
                                {
                                    scriptBytesToInsert = Array.Empty<byte>();
                                }
                                else
                                {
                                    scriptBytesToInsert = WriteData.SubentrMetadata[parentSubentryId].ScriptAsBytes;
                                }
                            }
                            else
                            {
                                if (WriteData.SubentryWriteLocationsInfo.ContainsKey(sameAsId))
                                {
                                    scriptBytesToInsert = Array.Empty<byte>();
                                }
                                else
                                {
                                    scriptBytesToInsert = WriteData.SubentrMetadata[sameAsId].ScriptAsBytes;
                                }
                            }
                        }
                        //////////////////////////////////////////
                        /// SameAs logic End
                        //////////////////////////////////////////
                        
                        if (lineId >= Setting.Settings.Pointers.Write[entryOwner].PointerFormat.EntryNumber)
                        {
                            throw new IndexOutOfRangeException($"Line key {lineId} out of range. " + 
                               $"Must be less than { Setting.Settings.Pointers.Write[entryOwner].PointerFormat.EntryNumber}. " + 
                               $"{nameof(entryOwner)}: {entryOwner}; " + 
                               $"Address: {MyMath.DecToHex(pointerTableLocation, Prefix.X)}");
                        }

                        if (Global.Debug)
                        {
                            Console.WriteLine($"Line number: {lineId}");
                        }

                        /*
                         * Enter the loop if we've run out of space in the write region. 
                         * This is a loop in case the next write region is not big enough 
                         * for some reason.
                         */

                        string region2Write2 = "";

                        if (placementStrategy == Enums.PlacementStrategy.Bestfit)
                        {
                            writeRegionI = 0;
                        }

                        while (true)
                        {
                            int lineLength = (scriptBytesToInsert == null ? 0 : scriptBytesToInsert.Length);

                            string range = writeRegionEntry.WriteableAddressRanges[writeRegionI];

                            if (!Setting.Settings.Write.WriteableRange.ContainsKey(range))
                            {
                                throw new Exception($"Invalid {nameof(Setting.Settings.Write.WriteableRange)}: {range}");
                            }

                            var writeRegionLength = Setting.Settings.Write.WriteableRange[range].Size;

                            // Check if need to migrate to the next available space slot.
                            if (Setting.Settings.Write.WriteableRange[range].WriteType != Enums.WriteType.Normal || WriteData.WriteRegionSpaceUsed[writeRegionEntry.WriteableAddressRanges[writeRegionI]] + lineLength <= writeRegionLength)
                            {
                                break;
                            }

                            // When StoreOutOfOrderToSaveSpace is set to true, this can get really noisy, so reserve it for debug.
                            if ((placementStrategy == Enums.PlacementStrategy.InOrder && Global.Verbose) || (placementStrategy == Enums.PlacementStrategy.Bestfit && Global.Debug))
                            {
                                Console.WriteLine($"Writable region name: {writeRegionI}");
                                Console.WriteLine($"Free space in region: {MyMath.DecToHex(bytesLost += scriptBytesToInsert.Length, Prefix.X)}");
                            }

                            if (++writeRegionI >= writeRegionEntry.WriteableAddressRanges.Length)
                            {
                                var freeSpace = Setting.Settings.Write.WriteableRange[writeRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Size - Setting.Settings.Write.WriteableRange[writeRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Size;
                                int linesLeftToInsert = linesSorted.Count + 1;
                                int uninsertedDataSize = 0;
                                uninsertedDataSize += kvp.Value.ScriptAsBytes.Length;

                                while (linesSorted.Count != 0)
                                {
                                    uninsertedDataSize += linesSorted.Pop().Value.ScriptAsBytes.Length;
                                }

                                throw new Exception(
                                    $"Not enough space.\n" +
                                    $"Entry owner name:        {entryOwner}\n" +
                                    $"Line id:                 {kvp.Value.Id}\n" +
                                    $"Lines left:              {linesLeftToInsert}\n" +
                                    $"Write subregion Size:    0x{Setting.Settings.Write.WriteableRange[writeRegionEntry.WriteableAddressRanges[writeRegionI - 1]].Size:X4} bytes\n" +
                                    $"Size of uninserted data: 0x{uninsertedDataSize:X4} bytes\n" +
                                    $"Free space in region:    0x{freeSpace:X4} bytes\n" +
                                    $"Space overflow:          0x{uninsertedDataSize - freeSpace:X4} bytes\n"
                                    );
                            }

                            /*
                             * Placement strategy log outputs different by flags
                             * This is because "Bestfit" is extremely noisy so this is reserved for the "Debug" output, which is noisy as it is.
                             */
                            if ((placementStrategy == Enums.PlacementStrategy.InOrder && Global.Verbose) || (placementStrategy == Enums.PlacementStrategy.Bestfit && Global.Debug))
                            {
                                Console.WriteLine($"Write region: {writeRegionI}");
                            }
                        }

                        region2Write2 = writeRegionEntry.WriteableAddressRanges[writeRegionI];

                        /*
                         * UsePointerFromID is for when two entries have the same text and use the same pointer.
                         * We want the inserter to make sure the pointers match when updating the pointers.
                         */

                        var subentryMetadata = WriteData.SubentrMetadata[subentryId];
                        long pcAddressOfPlaceToWrite;

                        if (!string.IsNullOrEmpty(subentryMetadata.SubentrySameAs))
                        {
                            // Todo: validate if a sameas entry is valid (like if they belong to different write regions)

                            // if the entry is the sameas another entry, set its entry value equal to it.
                            // If it exists that is. Otherwise, it's vice versa.
                            if (WriteData.SubentryWriteLocationsInfo.ContainsKey(subentryMetadata.SubentrySameAs))
                            {
                                WriteData.SubentryWriteLocationsInfo.Add(subentryId, WriteData.SubentryWriteLocationsInfo[subentryMetadata.SubentrySameAs]);
                            }
                            else
                            {
                                var sameAs = subentryMetadata.SubentrySameAs;

                                // calculate pointer based of LabelLocation

                                // Is is a label (something that points in the middle of a line)
                                if (WriteData.SubentrLabelMetadata.ContainsKey(sameAs))
                                {
                                    var parentSubentryId = WriteData.SubentrLabelMetadata[sameAs].Id;

                                    // If the parent subentry has already been written.
                                        if (WriteData.SubentryWriteLocationsInfo.ContainsKey(parentSubentryId))
                                    {
                                        var parentTextLocation = WriteData.SubentryWriteLocationsInfo[parentSubentryId].Location;
                                        pcAddressOfPlaceToWrite = parentTextLocation + WriteData.SubentrMetadata[parentSubentryId].Labels[sameAs].LabelLocation;
                                        WriteData.SubentryWriteLocationsInfo.Add(subentryId, new(region2Write2, pcAddressOfPlaceToWrite));

                                        //Don't write script data as it was already written
                                        // The below is a sanity check.
                                        if (scriptBytesToInsert.Length != 0)
                                        {
                                            throw new Exception($"{nameof(scriptBytesToInsert)} should be empty.");
                                        }
                                    }
                                    else
                                    {
                                        // Write parent pointer data (we'll write it's script data later.
                                        pcAddressOfPlaceToWrite = WriteData.WriteRegionSpaceUsed[region2Write2] + Setting.Settings.Write.WriteableRange[region2Write2].StartAddress;
                                        WriteData.SubentryWriteLocationsInfo.Add(parentSubentryId, new(region2Write2, pcAddressOfPlaceToWrite));

                                        // Write label pointer data that's a subset of the subentry.
                                        pcAddressOfPlaceToWrite += WriteData.SubentrLabelMetadata[sameAs].Labels[sameAs].LabelLocation;
                                        WriteData.SubentryWriteLocationsInfo.Add(subentryId, new(region2Write2, pcAddressOfPlaceToWrite));

                                        // Write script data which is pointed to by both pointers.
                                        WriteData.WriteRegionSpaceUsed[region2Write2] += scriptBytesToInsert.Length;
                                    }
                                }
                                else if (WriteData.SubentrMetadata.ContainsKey(sameAs))
                                {
                                    // Was the entry it's referencing already inserted?
                                    if (WriteData.SubentryWriteLocationsInfo.ContainsKey(sameAs))
                                    {
                                        // If so, set the current entry's write location to that location
                                        WriteData.SubentryWriteLocationsInfo.Add(subentryId, new(region2Write2, WriteData.SubentryWriteLocationsInfo[sameAs].Location));

                                        //Don't write script data as it was already written
                                        // The below is a sanity check.
                                        if (scriptBytesToInsert.Length != 0)
                                        {
                                            throw new Exception($"{nameof(scriptBytesToInsert)} should be empty.");
                                        }
                                    }
                                    else
                                    {
                                        // Update both pointer entries

                                        pcAddressOfPlaceToWrite = WriteData.WriteRegionSpaceUsed[region2Write2] + Setting.Settings.Write.WriteableRange[region2Write2].StartAddress;

                                        // Write pointer data (we'll write it's script data later.
                                        WriteData.SubentryWriteLocationsInfo.Add(sameAs, new(region2Write2, pcAddressOfPlaceToWrite));

                                        // Write subentry pointer data which is the same as the previous one.
                                        WriteData.SubentryWriteLocationsInfo.Add(subentryId, new(region2Write2, pcAddressOfPlaceToWrite));

                                        // Write script data which is pointed to by both pointers.
                                        WriteData.WriteRegionSpaceUsed[region2Write2] += scriptBytesToInsert.Length;
                                    }
                                }
                                else
                                {
                                    throw new Exception($"Invalid sameAs id: {subentryMetadata.SubentrySameAs}");
                                }
                            }
                        }
                        else
                        {
                            pcAddressOfPlaceToWrite = WriteData.WriteRegionSpaceUsed[region2Write2] + Setting.Settings.Write.WriteableRange[region2Write2].StartAddress;

                            // Check if it was before in the above codeblock.
                            if (!WriteData.SubentryWriteLocationsInfo.ContainsKey(subentryId))
                            {
                                WriteData.SubentryWriteLocationsInfo.Add(subentryId, new(region2Write2, pcAddressOfPlaceToWrite));
                            }

                            WriteData.WriteRegionSpaceUsed[region2Write2] += scriptBytesToInsert.Length;
                        }
                    }
                }

                // Write sublabel write locations
                foreach (var SubentryPointerKvp in WriteData.SubentrLabelMetadata)
                {
                    if (!writeRegionEntry.EntryOwners.Contains(SubentryPointerKvp.Value.IndexTable))
                    {
                        continue;
                    }

                    if (WriteData.SubentryWriteLocationsInfo.ContainsKey(SubentryPointerKvp.Key))
                    {
                        continue;
                    }

                    var parentWriteInfo = WriteData.SubentryWriteLocationsInfo[SubentryPointerKvp.Value.Id];
                    var location = parentWriteInfo.Location + SubentryPointerKvp.Value.Labels[SubentryPointerKvp.Key].LabelLocation;
                    WriteData.SubentryWriteLocationsInfo.Add(SubentryPointerKvp.Key, new(parentWriteInfo.WriteRangeId, location));
                }

                if (writeRegionEntry.EntryOwners.Length == 0)
                {
                    throw new Exception($"Can't have zero {nameof(writeRegionEntry.EntryOwners)}");
                }

                if (writeRegionEntry.WriteableAddressRanges.Length == 0)
                {
                    throw new Exception($"Can't have zero {nameof(writeRegionEntry.WriteableAddressRanges)}");
                }
            }

            timer.Stop();

            if (Global.Verbose)
            {
                Console.WriteLine($"Mapping write locations took: {TimeSpan.FromMilliseconds(timer.ElapsedMilliseconds).TotalSeconds} second(s).");
                Console.WriteLine($"Now inserting script.");
            }

            var writeToFileId = new List<string>();

            foreach (var indexTableInfoKvp in WriteData.IndexTableInfo)
            {
                foreach (var subEntryInfoKvp in indexTableInfoKvp.Value)
                {
                    if (!WriteData.SubentryWriteLocationsInfo.ContainsKey(subEntryInfoKvp.Key))
                    {
                        continue;
                    }

                    var subEntryInfo = subEntryInfoKvp.Value;
                    var settingsPointersWriteInfo = Setting.Settings.Pointers.Write[subEntryInfo.IndexTable];
                    var subentryWriteInfo = WriteData.SubentryWriteLocationsInfo[subEntryInfoKvp.Key];
                    var writeInfo = Setting.Settings.Write.WriteableRange[subentryWriteInfo.WriteRangeId];
                    var pointerGrouping = settingsPointersWriteInfo.PointerFormat.PointerGrouping;

                    if (Global.Debug)
                    {
                        Console.WriteLine($"Inserting entry: {subEntryInfo.Id}.");
                    }

                    bool writePointer;

                    if (settingsPointersWriteInfo.Disabled)
                    {
                        continue;
                    }

                    switch (pointerGrouping)
                    {
                        case Enums.PointerGrouping.Group:
                            writePointer = true;
                            break;
                        case Enums.PointerGrouping.SingleEntryFixedSize:
                        case Enums.PointerGrouping.Single:
                            writePointer = (subEntryInfoKvp.Value.EntryNo == 0);
                            break;
                        default:
                            throw new Exception($"Invalid pointer grouping vakue: {pointerGrouping}");
                    }

                    // Calculate and write pointer.
                    var pcAddressOfPlaceToWrite = subentryWriteInfo.Location;

                    if (writePointer) 
                    {
                        byte[] pointerBytes = null;

                        bool isValidPcAddress = true;
                        var convertedAddress = settingsPointersWriteInfo.PointerFormat.AddressConversion switch
                        {
                            Enums.AddrConversion.None => pcAddressOfPlaceToWrite,
                            Enums.AddrConversion.LoROM1 => AddressLoROM1.PcToSnes((int)pcAddressOfPlaceToWrite, out isValidPcAddress),
                            Enums.AddrConversion.LoROM2 => AddressLoROM2.PcToSnes((int)pcAddressOfPlaceToWrite, out isValidPcAddress),
                            Enums.AddrConversion.HiROM => AddressHiRom.PcToSnes((int)pcAddressOfPlaceToWrite, out isValidPcAddress),
                            Enums.AddrConversion.ExLoROM => AddressExLoROM.PcToSnes((int)pcAddressOfPlaceToWrite, out isValidPcAddress),
                            Enums.AddrConversion.ExHiROM => AddressExHiROM.PcToSnes((int)pcAddressOfPlaceToWrite, out isValidPcAddress),
                            _ => throw new Exception(),
                        };

                        if (!isValidPcAddress)
                        {
                            throw new Exception($"Address conversion failed. Not a valid PC address: 0x{pcAddressOfPlaceToWrite:X8}");
                        }

                        long diff = settingsPointersWriteInfo.PointerFormat.PcDifference - writeInfo.PcDifference;

                        if (convertedAddress - diff < 0)
                        {
                            throw new Exception("Address negative after subtraction. " + 
                                $"{MyMath.DecToHex(convertedAddress, Prefix.X)} - {MyMath.DecToHex(settingsPointersWriteInfo.PointerFormat.PcDifference, Prefix.X)} - " + 
                                $"{MyMath.DecToHex(writeInfo.PcDifference, Prefix.X)} = {MyMath.DecToHex(convertedAddress - diff, Prefix.X)}");
                        }

                        convertedAddress -= diff;

                        switch (settingsPointersWriteInfo.PointerFormat.PointerType)
                        {
                            case Enums.PointerType.LittleEndian:
                                pointerBytes = PointerManager.CreatePointerLittleEndian(convertedAddress, settingsPointersWriteInfo.PointerFormat.PointerLength);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimaryNoLength:
                                // Bits for this address are zeroed out when you're using a delimiter,
                                // so we pass the length as zero here.
                                pointerBytes = PointerManager.CreateAddressMetalSladerSNES(convertedAddress, 0);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerDXPrimary:
                                pointerBytes = PointerManager.CreateAddressMetalSladerSNES(convertedAddress, subEntryInfo.ScriptAsBytes.Length);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimaryNoLength:
                                pointerBytes = PointerManager.CreateAddressMetalSladerNES(convertedAddress, 0);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESPrimary:
                                pointerBytes = PointerManager.CreateAddressMetalSladerNES(convertedAddress, subEntryInfo.ScriptAsBytes.Length);
                                break;
                            case Enums.PointerType.ThreeByteMetalSladerNESTransPrimaryNoLength:
                                pointerBytes = PointerManager.CreateAddressMetalSladerNES(convertedAddress, 0, true);
                                break;
                            case Enums.PointerType.None:
                                break;
                            case Enums.PointerType.Custom:
                                pointerBytes = PointerManager.CreateAddressCustomFormat(convertedAddress, settingsPointersWriteInfo.PointerFormat.CustomPointerFormat);
                                break;
                            case Enums.PointerType.MoonPrincessTrans:
                                pointerBytes = PointerManager.CreateAddressMoonPrincessTrans(convertedAddress);
                                break;
                            case Enums.PointerType.FrogGameTrans:
                                pointerBytes = PointerManager.CreateAddressFrogGameTrans(convertedAddress);
                                break;
                            default:
                                throw new Exception();
                        }

                        if (Setting.Settings.Misc.GetFileToWritePointerFromPointersField)
                        {
                            FILE.CURRENT_FILE = settingsPointersWriteInfo.PointerFormat.File;
                        }
                        else
                        {
                            FILE.CURRENT_FILE = writeInfo.File;
                        }

                        if (!writeToFileId.Contains(FILE.CURRENT_FILE))
                        {
                            writeToFileId.Add(FILE.CURRENT_FILE);

                            if (writeInfo.WriteType == Enums.WriteType.NewFile)
                            {
                                FILE.DATA = Array.Empty<byte>();
                            }
                        }

                        if (subEntryInfoKvp.Value.SubentryIndex == 0)
                        {
                            // Write pointer bytes
                            var bytseBetween = Setting.Settings.Pointers.Write[subEntryInfoKvp.Value.IndexTable].PointerFormat.BytesBetween;
                            var entryNo = subEntryInfoKvp.Value.EntryNo;
                            var pointerTableLocation = subEntryInfoKvp.Value.PointerTableLocation;

                            if (pointerBytes == null)
                            {
                                pointerBytes = Array.Empty<byte>();
                            }

                            var pointerLocation = pointerTableLocation + pointerBytes.Length * entryNo + (entryNo * bytseBetween);

                            if (pointerLocation + pointerBytes.Length > FILE.DATA.Length)
                            {
                                throw new Exception($"{nameof(pointerLocation)}({MyMath.DecToHex(pointerLocation, Prefix.X)}) is greater than " +
                                    $"file's ({FILE.CURRENT_FILE}) size({MyMath.DecToHex(FILE.DATA.Length, Prefix.X)}).");
                            }

                            if (pointerLocation == 0x00B2C0 || pointerLocation == 0x0000B2DE)
                            {

                            }

                            Array.Copy(pointerBytes, 0, FILE.DATA, pointerLocation, pointerBytes.Length);
                        }

                        int i = 0;
                        WriteData.PointerEntry previousSubentry = null;
                        foreach (var subent in subEntryInfo.SubentryPointers)
                        {
                            if (i != 0 && previousSubentry.SubentryPointerLocation + previousSubentry.EmbeddedPointerFormat.PointerLength != subent.Value.SubentryPointerLocation )
                            {
                                i = 0;
                            }

                            previousSubentry = subent.Value;

                            var e = subent.Value.EmbeddedPointerFormat;

                            var address = WriteData.SubentryWriteLocationsInfo[subent.Value.GotoSubentryId].Location;

                            long pointerLocation = subent.Value.SubentryPointerLocation;

                            address = e.AddressConversion switch
                            {
                                Enums.AddrConversion.None => address,
                                Enums.AddrConversion.LoROM1 => AddressLoROM1.PcToSnes((int)address, out isValidPcAddress),
                                Enums.AddrConversion.LoROM2 => AddressLoROM2.PcToSnes((int)address, out isValidPcAddress),
                                Enums.AddrConversion.HiROM => AddressHiRom.PcToSnes((int)address, out isValidPcAddress),
                                Enums.AddrConversion.ExLoROM => AddressExLoROM.PcToSnes((int)address, out isValidPcAddress),
                                Enums.AddrConversion.ExHiROM => AddressExHiROM.PcToSnes((int)address, out isValidPcAddress),
                                _ => throw new Exception($"Invalid {nameof(e.AddressConversion)}: {e.AddressConversion}"),
                            };

                            if (!isValidPcAddress)
                            {
                                throw new Exception("Subentry address conversion failed.");
                            }

                            switch (e.AddressType)
                            {
                                case Enums.AddressType.Absolute:
                                    break;
                                case Enums.AddressType.Relative:
                                    address -= WriteData.SubentryWriteLocationsInfo[subEntryInfo.Id].Location + subent.Value.SubentryPointerLocation;
                                    break;
                                default:
                                    throw new Exception($"Invalid {nameof(e.AddressType)}: {e.AddressType}");
                            }

                            if (e.PcDifferenceExcludePointer)
                            {
                                if (address >= 0)
                                {
                                    address -= (e.EntryNumber - i) * e.PointerLength;
                                }
                                else
                                {
                                    address -= (i - e.EntryNumber) * e.PointerLength;
                                }
                            }

                            address -= e.PcDifference;

                            if (address < 0)
                            {
                                long maskNegative = 0xffffffff >> ((8 - e.PointerLength) * 8);
                                address ^= maskNegative ^ -1;
                            }

                            var byteArrayPtr = Array.Empty<byte>();

                            switch (e.PointerType)
                            {
                                case Enums.PointerType.None:
                                    break;
                                case Enums.PointerType.LittleEndian:
                                    byteArrayPtr = PointerManager.CreatePointerLittleEndian(address, e.PointerLength);
                                    break;
                                case Enums.PointerType.BigEndian:
                                    byteArrayPtr = PointerManager.CreatePointerBigEndian(address, e.PointerLength);
                                    break;
                                case Enums.PointerType.EarthboundEmbeddedPointer:
                                    goto case Enums.PointerType.LittleEndian;
                                case Enums.PointerType.Custom:
                                    byteArrayPtr = PointerManager.CreateAddressCustomFormat(convertedAddress, e.CustomPointerFormat);
                                    break;
                                default:
                                    throw new Exception($"Invalid {nameof(e.PointerType)}: {e.PointerType}");
                            }

                            Array.Copy(byteArrayPtr, 0, subEntryInfo.ScriptAsBytes, pointerLocation, byteArrayPtr.Length);
                            i++;
                        }
                    }

                    // Insert script bytes into rom file
                    FILE.CURRENT_FILE = writeInfo.File;

                    if (!writeToFileId.Contains(FILE.CURRENT_FILE))
                    {
                        writeToFileId.Add(FILE.CURRENT_FILE);

                        if (writeInfo.WriteType == Enums.WriteType.NewFile)
                        {
                            FILE.DATA = Array.Empty<byte>();
                        }
                    }

                    if (string.IsNullOrEmpty(subEntryInfo.SubentrySameAs))
                    {
                        var requiredSize = subentryWriteInfo.Location + subEntryInfo.ScriptAsBytes.Length;

                        if (requiredSize > FILE.DATA.Length)
                        {
                            switch (writeInfo.WriteType)
                            {
                                case Enums.WriteType.NewFile:
                                case Enums.WriteType.Expandable:
                                    var t = FILE.DATA;
                                    Array.Resize(ref t, (int)requiredSize);
                                    FILE.DATA = t;
                                    break;
                                case Enums.WriteType.Normal:
                                    throw new Exception();
                                default:
                                    throw new Exception();
                            }
                        }

                        Array.Copy(subEntryInfo.ScriptAsBytes, 0, FILE.DATA, subentryWriteInfo.Location, subEntryInfo.ScriptAsBytes.Length);
                    }
                }
            }

            foreach (string ss in writeToFileId)
            {
                File.WriteAllBytes(Setting.Settings.Files[ss], FILE.ALL[ss]);
            }
        }
    }
}