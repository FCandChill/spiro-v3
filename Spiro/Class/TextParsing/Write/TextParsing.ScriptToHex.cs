﻿using Spiro.Class;
using Spiro.Class.PointerManagement;
using Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static Structs.Enums;

namespace TextParse
{
    partial class TextParsing
    {
        public byte[] ConvertScriptToHexWithDictionary(string line, string entryId, PointerReadWrite prw, long pointerTableKey)
        {
            Chara.QuickChange(false, prw.RenderFormat.TableMain.Value);

            line = Regex.Replace(line, @"\\.*?/", "");

            DictionaryTables[] dictionaryInteractionType = prw.RenderFormat.DictionaryInteractions;
            bool reverseTableEndianness = prw.RenderFormat.ReverseTableEndianness;

            List<byte> output = new List<byte>();
            Regex regex1 = new Regex(Constants.REGEX_SPLIT_STRING);

            string[] aSubChunk = regex1.Split(line).Where(s => s != string.Empty).ToArray();

            Dictionary<string, byte[]> controlCodeByString, dictionaryByString, chrByString;

            int biggestEntry;
            string potentialSubentryMarker = "";

            for (int i = 0; i < aSubChunk.Length;)
            {
                controlCodeByString = new();
                dictionaryByString = new();
                chrByString = new();

                biggestEntry = Constants.GENERIC_NULL;
                string chunk = aSubChunk[i];
                byte[] add = null;

                bool isValidChar = Chara.DictSortedByString.ContainsKey(chunk) || Chara.CHRSortedByString.ContainsKey(chunk);

                if (isValidChar)
                {
                    if ((dictionaryInteractionType.Contains(DictionaryTables.Dictionary) || dictionaryInteractionType.Contains(DictionaryTables.ChrDictionary) || dictionaryInteractionType.Contains(DictionaryTables.ControlCode)) && Chara.DictSortedByString.ContainsKey(chunk))
                    {
                        byte[] id = Chara.DictSortedByString[chunk].ByKey.ElementAt(0).Key;
                        if (Chara.DictControlcode.ContainsKey(id))
                        {
                            controlCodeByString.Add(Chara.DictControlcode[id].StringRepresentation, id);
                            // Not sure why it's set to 1 for control codes.
                            biggestEntry = 1;
                        }
                        else
                        {
                            dictionaryByString = Chara.DictSortedByString[chunk].ByString.OrderByDescending(x => x.Key.Length).ToDictionary(x => x.Key, x => x.Value);
                            biggestEntry = Chara.DictSortedByString[chunk].MaxNumberOfCharacters;
                        }
                    }

                    if (dictionaryInteractionType.Contains(DictionaryTables.CHR) && Chara.CHRSortedByString.ContainsKey(chunk))
                    {
                        chrByString = Chara.CHRSortedByString[chunk].ByString.OrderByDescending(x => x.Key.Length).ToDictionary(x => x.Key, x => x.Value);
                        biggestEntry = Math.Max(Chara.CHRSortedByString[chunk].MaxNumberOfCharacters, biggestEntry);
                    }

                    int lengthToTry = biggestEntry;
                    while (!(i + lengthToTry <= aSubChunk.Length))
                    {
                        lengthToTry--;
                    }

                    var found = DictionaryTables.Null;
                    var lookMeUp = "";

                    while (found == DictionaryTables.Null && lengthToTry > 0)
                    {
                        string[] ss = new string[lengthToTry];
                        Array.Copy(aSubChunk, i, ss, 0, lengthToTry);
                        lookMeUp = string.Join("", ss);

                        if (controlCodeByString.ContainsKey(lookMeUp))
                        {
                            found = DictionaryTables.ControlCode;
                            break;
                        }

                        if (dictionaryByString.ContainsKey(lookMeUp))
                        {
                            found = DictionaryTables.Dictionary;
                            break;
                        }

                        if (chrByString.ContainsKey(lookMeUp))
                        {
                            found = DictionaryTables.CHR;
                            break;
                        }

                        lengthToTry--;
                    }

                    if (lengthToTry <= 0)
                    {
                        throw new Exception($"{nameof(lengthToTry)} exhausted. Character to look for not found. Chunk: {chunk}");
                    }

                    switch (found)
                    {
                        case DictionaryTables.ControlCode:
                            var id = controlCodeByString[lookMeUp];
                            add = id;

                            var enoughArguments = true;
                            var ControlCodeArgumentLength = Chara.DictControlcode[id].CodeArgumentLength;
                            var tabelEntry = Chara.DictControlcode[id].TableEntry;

                            if (i + ControlCodeArgumentLength < aSubChunk.Length)
                            {
                                for (int j = i + 1; j < i + 1 + ControlCodeArgumentLength; j++)
                                {
                                    if (LabelMatchRegex.IsMatch(aSubChunk[j]))
                                    {
                                        continue;
                                    }
                                    
                                    if (!ByteMatchRegex.IsMatch(aSubChunk[j]))
                                    {
                                        enoughArguments = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                enoughArguments = false;
                            }

                            if (tabelEntry != "")
                            {
                                Chara.QuickChange(false, prw.RenderFormat.Tables[tabelEntry]);
                            }

                            if (!enoughArguments)
                            {
                                throw new Exception($"Entry {entryId}'s control code \"{aSubChunk[i]}\" didn't receive the amount of required argument bytes: {ControlCodeArgumentLength}. PointerTableKey: {MyMath.DecToHex(pointerTableKey, Prefix.X)}");
                            }

                            break;
                        case DictionaryTables.Dictionary:
                            add = Chara.DictSortedByString[chunk].ByString[lookMeUp];
                            break;
                        case DictionaryTables.CHR:
                            add = Chara.CHRSortedByString[chunk].ByString[lookMeUp];
                            break;
                        default:
                            throw new Exception("Invalid dictionary table value.");
                    }
                    i += lengthToTry;
                }
                // is hex value
                else if (new Regex("^({[A-Za-z0-9]{2}})$").IsMatch(chunk))
                {
                    var match = (new Regex(@"(?<={)(?<byte>[A-Za-z0-9]{2})(?=})")).Match(chunk);

                    if (!match.Success)
                    {
                        throw new Exception($"Unable to extract byte data from: {chunk}");
                    }

                    int hexValue = MyMath.HexToDec(match.Groups["byte"].Value);
                    add = new byte[] { (byte)hexValue };
                    i++;
                }
                // is subentry label / pointer
                else if (chunk.StartsWith("<"))
                {
                    var match = new Regex("(?<=id=\")(?<idField>.*?)(?=\")").Match(chunk);

                    if (!match.Success)
                    {
                        throw new Exception($"Unable to extract id from tag: {chunk}");
                    }

                    var id = match.Groups["idField"].Value;

                    switch (chunk)
                    {
                        case string a when new Regex("^<subentryPointer id=\".*?\">$").IsMatch(a):

                            /*
                             * Validate the marker before the subentryPointer
                             * And also get the proper EmbeddedPointerFormat object
                            */
                            EmbeddedPointerFormat format = null;
                            foreach (var c in prw.EmbeddedPointer.Formats)
                            {
                                if (c.ControlCode == potentialSubentryMarker)
                                {
                                    format = c;
                                    break;
                                }
                            }

                            if (format == null)
                            {
                                throw new Exception($"Redirect isn't procededed by a valid marker: {potentialSubentryMarker}");
                            }

                            if (!WriteData.SubentrMetadata[entryId].SubentryPointers.ContainsKey(i)) 
                            {
                                WriteData.SubentrMetadata[entryId].SubentryPointers.Add(i, new(id, output.Count, format));
                            }

                            // Write temporary filler bytes so we can write pointer infomration to them later.
                            add = Array.Empty<byte>();
                            for (int j = 0; j < format.PointerLength; j++)
                            {
                                output.Add(0x62);
                            }
                            break;
                        case string b when LabelMatchRegex.IsMatch(b):
                            if (!WriteData.SubentrMetadata[entryId].Labels.ContainsKey(id))
                            {
                                WriteData.SubentrMetadata[entryId].Labels.Add(id, new(output.Count));
                            }
                            else
                            {
                                WriteData.SubentrMetadata[entryId].Labels[id] = new(output.Count);
                            }

                            if (!WriteData.SubentrLabelMetadata.ContainsKey(id))
                            {
                                WriteData.SubentrLabelMetadata.Add(id, WriteData.SubentrMetadata[entryId]);
                            } 
                            else
                            {
                                WriteData.SubentrLabelMetadata[id] = WriteData.SubentrMetadata[entryId];
                            }

                            add = Array.Empty<byte>();
                            break;
                        default:
                            break;
                    }
                    i++;
                }
                else
                {
                    throw new Exception($"Entry id: {entryId}. PointerTableKey: {MyMath.DecToHex(pointerTableKey, Prefix.X)}. Bad chunk \"{chunk}\". Not in the tables in DictionaryInteractions.");
                }

                if (!chunk.StartsWith("<"))
                {
                    potentialSubentryMarker = chunk;
                }

                if (reverseTableEndianness)
                {
                    add = add.Reverse().ToArray();
                }

                output = output.Concat(add).ToList();
            }

            return output.ToArray();
        }

        public byte[] ConvertScriptToHexMarioPicrossSNES(string line, string entryId, PointerReadWrite prw, long pointerTableKey)
        {
            /*
             * When dumping the Mario picross text we dump it as follows:
             * 
             * 1. Bunch of bytes (at end, is the puzzle name length)
             * 2. Puzzle name
             * 3. More bytes
             * 
             * Our goal is to extract the puzzle name and update the length bytes in part 1.
             */

            if (line == "")
            {
                return Array.Empty<byte>();
            }

            string[] apuzzleName = (new Regex(@"({[A-Za-z0-9]{2}})+")).Split(line).Where(s => s != String.Empty).ToArray();

            List<string> puzzleBytes = new();

            foreach (Match m in (new Regex(@"({[A-Za-z0-9]{2}})+")).Matches(line))
            {
                if (m.Success)
                {
                    puzzleBytes.Add(m.Value);
                }
            }

            if (puzzleBytes.Count != 2 && apuzzleName.Length != 3)
            {
                throw new Exception($"Unable to split string: entryId: {entryId}");
            }

            string puzzleName = apuzzleName[1];

            // Calculate the puzzle name byte length
            int puzzleNameLength = ConvertScriptToHexWithDictionary(puzzleName, entryId, prw, pointerTableKey).Length + 4;

            // .Substring(0, puzzleBytes[0].Length - 8);
            string puzzleBytes0Part1 = puzzleBytes[0][0..^8];
            string formattedLength = MyMath.FormatByteInBrackets((byte)(puzzleNameLength & 0xFF)) + MyMath.FormatByteInBrackets((byte)(puzzleNameLength & 0xFF00));

            string newLine = puzzleBytes0Part1 + formattedLength + puzzleName + puzzleBytes[1];

            return ConvertScriptToHexWithDictionary(newLine, entryId, prw, pointerTableKey).ToArray();
        }

        public byte[] ConvertScriptToHexTensaiBakabon(string line, string entryId, PointerReadWrite prw, long pointerTableKey)
        {
            line += "[LINE]";

            var lineBreaks = (new Regex(@"\[LINE\]")).Matches(line).Count;

            line = MyMath.FormatByteInBrackets((byte)lineBreaks) + line;

            return ConvertScriptToHexWithDictionary(line, entryId, prw, pointerTableKey).ToArray();
        }

        public byte[] ConvertScriptToHexLengthAtBeginning(string line, string entryId, PointerReadWrite prw, long pointerTableKey, long difference)
        {
            var convertedScript = ConvertScriptToHexWithDictionary(line, entryId, prw, pointerTableKey).ToArray();

            if (convertedScript.Length + difference > byte.MaxValue)
            {
                throw new NotImplementedException("ConvertScriptToHexLengthAtBeginning doesn't support multi-byte length identifiers.");
            }

            return new byte[] { (byte)(convertedScript.Length + difference) }.Concat(convertedScript).ToArray();
        }
    }
}