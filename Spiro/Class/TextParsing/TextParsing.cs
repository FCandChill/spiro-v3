﻿using Spiro.Class;
using Structs;
using System.Collections.Generic;
using static Structs.RenderFormat;

namespace TextParse
{
    partial class TextParsing
    {
        public readonly MyDictionary Chara;

        public TextParsing()
        {
            Chara = new MyDictionary();
        }

        public void LoadNewTableFile(bool readOriginal, RenderFormat.TableEntry te, KeyValuePair<string, TableEntry> mainTe)
        {
            Chara.LoadTableFiles(readOriginal, te, mainTe);
        }

        public bool QuickChange(bool readOriginal, TableEntry te) => Chara.QuickChange(readOriginal, te);

        public void AddValueToDictionary(byte[] addEntryToDictionary)
        {
            Chara.AddValueToDictionary(addEntryToDictionary);
        }
    }
}