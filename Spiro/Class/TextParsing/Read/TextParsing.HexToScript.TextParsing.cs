﻿using Spiro.Class;
using Structs;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TextParse
{
    public partial class TextParsing
    {
        private readonly Regex ByteMatchRegex = new Regex(@"{.*?}");
        private readonly Regex LabelMatchRegex = new Regex("^<label id=\".*?\">$");

        public string BytesToStringKaettekitaMarioBrothers(
            List<byte> compressed,
            long metadataStartAddressOffset,
            long entryNo,
            string key,
            long pointerLocation,
            PointerReadWrite prw
            )
        {
            if (compressed.Count == 0)
            {
                return "";
            }

            if (compressed.Count == 1)
            {
                return MyMath.FormatByteInBrackets(compressed[0]);
            }

            // Make sure the first two bytes are printed as bytes in the string
            string Line = MyMath.FormatByteInBrackets(compressed[0]) + MyMath.FormatByteInBrackets(compressed[1]);
            compressed.RemoveAt(0);
            compressed.RemoveAt(0);
            ReadByteStreamArgs rba = new ReadByteStreamArgs(
                EntryNo: entryNo,
                StartTextAddress: 0,
                Key: key,
                PointerLocation: pointerLocation,
                Prw: prw,
                Buffer: compressed.ToArray(),
                IncludeRedirects: prw.RenderFormat.IncludeRedirects,
                MetadataStartAddressOffset: metadataStartAddressOffset
                );
            return Line + GetSingleEntry(ReadByteStreamWithPointer(rba));
        }

        public string BytesToStringMetalSladerNameCards(
            byte[] scriptAsBytes,
            long metadataStartAddressOffset,
            string key,
            long pointerLocation,
            long entryNo,
            PointerReadWrite prw
            )
        {
            if (prw.RenderFormat.DialogueReadType != Enums.DialogueReadType.HasLengthVar)
            {
                throw new Exception($"{nameof(prw.RenderFormat.DialogueReadType)} must be {nameof(Enums.DialogueReadType.HasLengthVar)}");
            }

            string scriptAsText = "";

            if (scriptAsBytes.Length > 1)
            {
                const byte METAL_SLADER_CODE = 0x00;

                if (scriptAsBytes[0] == METAL_SLADER_CODE)
                {
                    //If 00 00 
                    //Then it's just bytes of code.
                    if (scriptAsBytes[1] == METAL_SLADER_CODE)
                    {
                        foreach (byte b in scriptAsBytes)
                        {
                            scriptAsText += MyMath.FormatByteInBrackets(b);
                        }
                    }
                    //Otherwise, it's a namecard.
                    else
                    {
                        scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[0]);
                        scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[1]);

                        int NameCardSize = scriptAsBytes[1];
                        byte[] namecard = new byte[NameCardSize];
                        Array.Copy(scriptAsBytes, 2, namecard, 0, NameCardSize);

                        var rbr = ReadByteStreamWithPointer(
                            new ReadByteStreamArgs(entryNo,
                            0,
                            key,
                            pointerLocation,
                            prw,
                            namecard,
                            prw.RenderFormat.IncludeRedirects,
                            UsesDelimiter: false,
                            LogMetadata: false,
                            ByteSize: namecard.Length,
                            MetadataStartAddressOffset: metadataStartAddressOffset
                            )
                         );
                        scriptAsText += GetSingleEntry(rbr);

                        for (int i = NameCardSize + 2; i < scriptAsBytes.Length; i++)
                        {
                            scriptAsText += MyMath.FormatByteInBrackets(scriptAsBytes[i]);
                        }
                    }
                }
                else
                {
                    var rbr = ReadByteStreamWithPointer(
                        new ReadByteStreamArgs(
                            entryNo,
                            0,
                            key,
                            pointerLocation,
                            prw,
                            scriptAsBytes,
                            prw.RenderFormat.IncludeRedirects,
                            UsesDelimiter: false,
                            LogMetadata: false,
                            ByteSize: scriptAsBytes.Length,
                            MetadataStartAddressOffset: metadataStartAddressOffset
                            )
                        );
                    scriptAsText += GetSingleEntry(rbr);
                }
            }
            else if (scriptAsBytes.Length == 1)
            {
                var rbr = ReadByteStreamWithPointer(
                    new ReadByteStreamArgs(
                        entryNo, 
                        0,
                        key,
                        pointerLocation,
                        prw,
                        scriptAsBytes,
                        prw.RenderFormat.IncludeRedirects,
                        UsesDelimiter: false,
                        LogMetadata: false,
                        ByteSize: scriptAsBytes.Length,
                        MetadataStartAddressOffset: metadataStartAddressOffset
                     ));
                scriptAsText += GetSingleEntry(rbr);
            }
            return scriptAsText;
        }

        /// <summary>
        /// GetSingleEntry is a hackish way of returning a single subentry from an rbr dictionary, which can store multiple subentries.
        /// Render functions are extremely game specific. The game's that use this function don't have any sort of 
        /// embeded redirects / subentry functionality.
        /// 
        /// As a result, these render functions do some calculations and eventually call "ReadByteStreamWithPointer" (a multi-purpose function)
        /// when a certain byte sequence needs to be rendered as text.
        /// 
        /// This function simply extracts the string from the first index of rbr as well as preforms some sanity
        /// checks to ensure the return matches our assumptions.
        /// </summary>
        /// <returns></returns>
        private static string GetSingleEntry(Dictionary<int, ReadByteStreamReturn> rbr)
        {
            const int depth = 0;

            if (rbr.Count != 1)
            {
                throw new Exception("rbr must be a length of 1. Please verify your rendering function is configured properly and disables redirects.");
            }

            if (rbr[depth].SameAsId != null)
            {
                throw new Exception("rbr's sameAsId is not empty. Please verify your rendering function is configured properly and disables redirects.");
            }

            return rbr[depth].ScriptString;
        }
    }
}