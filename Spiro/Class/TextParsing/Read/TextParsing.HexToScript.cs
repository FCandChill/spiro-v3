﻿using LunarAddr;
using Spiro.Class;
using Spiro.Class.PointerManagement;
using Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static Structs.Enums;

namespace TextParse
{
    public partial class TextParsing
    {
        private readonly Regex endsWithLabel = new("<label id=\".*?\">$");
        public Dictionary<int, ReadByteStreamReturn> ReadByteStreamWithPointerInSizeAtBeginning(ReadByteStreamArgs rba, long lengthOfLengthIdentier, bool addOne)
        {
            static byte[] PadOutArray(byte[] array, int length)
            {
                var newArray = new byte[length];
                var startAt = 0;
                Buffer.BlockCopy(array, 0, newArray, startAt, array.Length);
                return newArray;
            }

            // don't go above 4 bytes as that's the limit for uint
            const int maxLength = 4;
            if (lengthOfLengthIdentier > maxLength)
            {
                throw new Exception($"Pointer at beginning cannot be greater than {maxLength}.");
            }

            byte[] lengthBytes = new byte[lengthOfLengthIdentier];
            Array.Copy(FILE.DATA, (int)rba.StartTextAddress, lengthBytes, 0, lengthOfLengthIdentier);

            // bitconvert expects an array of a fixed size, so pad out the lengthBytes array with zeros at the end
            long length = BitConverter.ToUInt32(PadOutArray(lengthBytes, maxLength), 0);

            if (addOne)
            {
                length++;
            }

            const int depth = 0;

            // Process the entry and ignore the length bytes at the beginning
            rba.StartTextAddress += lengthOfLengthIdentier;
            rba.ByteSize = length - lengthOfLengthIdentier;
            var rbr = ReadByteStreamWithPointer(rba);

            // Correct the metadata because we ignored the length bytes at the beginning.
            rbr[depth].ScriptBytes = lengthBytes.Concat(rbr[depth].ScriptBytes).ToList();
            rbr[depth].ScriptBytesWithoutDelimiter = lengthBytes.Concat(rbr[depth].ScriptBytesWithoutDelimiter).ToList();

            return rbr;
        }

        /// <summary>
        /// ReadByteStreamArgs contains the standard information to feed into bytestream reading code. Instead of specify each item as an argument, 
        /// they can be consolidated into a single class.
        /// </summary>
        public class ReadByteStreamArgs
        {
            public long EntryNo { get; set; }

            /* 
             * There's two StartTextAddress variables.
             * This is for custom DialogueReadType routines
             * where there's extra byte data before the actual text data we want to dump.
             * 
             * InitialStartTextAddress is used for accurate text blanking as well 
             * accurate sameas dictionaries
             */
            public long InitialStartTextAddress { get; set; }
            public long StartTextAddress { get; set; }
            public string Key { get; set; }
            public long PointerLocation { get; set; }
            public PointerReadWrite Prw { get; set; }
            public byte[] Buffer { get; set; }
            /// <summary>
            /// indicates as to whether or not to stop reaching once a delimiter value has been reached.
            /// </summary>
            public bool UsesDelimiter { get; set; }
            /// <summary>
            /// If ByteSize is set, then we stop reading if we've read X amount of bytes.
            /// </summary>
            public long ByteSize { get; set; }

            public bool IncludeRedirects { get; set; }
            public bool LogMetadata { get; set; }
            public long MetadataStartAddressOffset { get; set; }

            public ReadByteStreamArgs(
                long EntryNo,
                long StartTextAddress,
                string Key,
                long PointerLocation,
                PointerReadWrite Prw,
                byte[] Buffer,
                bool IncludeRedirects,
                bool UsesDelimiter = false,
                bool LogMetadata = true,
                long ByteSize = Constants.GENERIC_NULL,
                long MetadataStartAddressOffset = 0
                )
            {
                this.EntryNo = EntryNo;
                this.InitialStartTextAddress = this.StartTextAddress = StartTextAddress;
                this.Key = Key;
                this.PointerLocation = PointerLocation;
                this.Prw = Prw;
                this.Buffer = Buffer;
                this.UsesDelimiter = UsesDelimiter;
                this.ByteSize = ByteSize;
                this.IncludeRedirects = IncludeRedirects;
                this.LogMetadata = LogMetadata;
                this.MetadataStartAddressOffset = MetadataStartAddressOffset;
            }

            public ReadByteStreamArgs Clone()
            {
                return (ReadByteStreamArgs)MemberwiseClone();
            }
        }

        public class ReadByteStreamReturn
        {
            public string Id = Guid.NewGuid().ToString("N");
            public List<byte> ScriptBytes = new List<byte>();
            public List<byte> ScriptBytesWithoutDelimiter = new List<byte>();
            public string ScriptString = "";
            public long TextLocation = 0;
            public string SameAsId;

            /// <summary>
            /// This essentially indicates that a subentry references another subentry label.
            /// In this case, we cannot delete the subentry as it's used for further calculations.
            /// However, we also don't want to dump is text it to the json script dump as it's functionally useless.
            /// 
            /// Take this for example:
            /// dialogue1_000051: "Text [goto]<subentryPointer id=\"dialogue1_000052\">"
            /// dialogue1_000052: " text"
            /// dialogue1_000055: "text <label id=\"dialogue1_000052\"> text"
            /// 
            /// We've programmed the script dumper in a way where a text line that's referenced at multiple
            /// pointers isn't split up into different subentries. It makes scripts more readable.
            /// </summary>
            public bool IsSubentryLabel = false;

            public ReadByteStreamReturn(string prefix)
            {
                if (string.IsNullOrEmpty(prefix))
                {
                    SetIdToGuid();
                    return;
                }

                if (!DmpData.SubentryIdCounter.ContainsKey(prefix))
                {
                    DmpData.SubentryIdCounter.Add(prefix, 0);
                }

                Id = $"{prefix}_{DmpData.SubentryIdCounter[prefix]++:000000}";
            }

            public ReadByteStreamReturn()
            {
                SetIdToGuid();
            }

            private void SetIdToGuid()
            {
                Id = Guid.NewGuid().ToString("N");
            }
        }

        public Dictionary<int, ReadByteStreamReturn> ReadByteStreamWithPointer(ReadByteStreamArgs rba)
        {
            Dictionary<int, ReadByteStreamReturn> rbr = new Dictionary<int, ReadByteStreamReturn>();
            return ReadByteStreamWithTextLocation(rba, rbr);
        }

        /// <summary>
        /// A complex function that takes a text locaitons, reads byte data until logic prompts it
        /// to stop (whether that be the byte stream ends or a delimiter indicates to stop).
        /// Additionally, this function translates byte data to text based on the CHR and dictionary files.
        /// Additionally, it processes embedded pointers.
        /// </summary>
        /// <param name="rba"></param>
        /// <param name="rbr"></param>
        /// <returns></returns>
        private Dictionary<int, ReadByteStreamReturn> ReadByteStreamWithTextLocation(ReadByteStreamArgs rba, Dictionary<int, ReadByteStreamReturn> rbr)
        {
            int depth = 0;

            if (!rbr.ContainsKey(depth))
            {
                rbr.Add(depth, new ReadByteStreamReturn(rba.Prw.PointerFormat.SubentryKeyPrefix));
            }

            rbr[depth].TextLocation = rba.InitialStartTextAddress;

            var key = (rba.Prw.RenderFormat.File, rba.InitialStartTextAddress);
            if (rba.LogMetadata && !DmpData.SubentryLocations.Keys.Contains(key))
            {
                DmpData.SubentryLocations.Add(key, new SubentryLocationsValue());
                DmpData.SubentryLocations[key].ParentAddress = rba.InitialStartTextAddress;
                DmpData.SubentryLocations[key].Id = rbr[depth].Id;
            }

            if (rba.LogMetadata && DmpData.PassNumber == 0)
            {
                DmpData.SubentrMetadata.Add(rbr[depth].Id, new());
            }

            ReadByteStreamWithPointerRecursive(rba, ref rbr, depth);
            return rbr;
        }

        public void ReadByteStreamWithPointerRecursive(ReadByteStreamArgs rba, ref Dictionary<int, ReadByteStreamReturn> rbr, int depth)
        {
            Chara.QuickChange(true, rba.Prw.RenderFormat.TableMain.Value);

            var action = Enums.ControlCodeAction.PrintText;
            if (Global.Debug)
            {
                Console.WriteLine($"Start of decompressing subentry #{depth}");
            }

            // Make the ids match our first run through.
            {
                (string File, long PointerLocation, long StartTextAddress) locationKey;

                if (rba.Prw.PointerFormat.PointerGrouping != PointerGrouping.Single)
                {
                    locationKey = (rba.Prw.RenderFormat.File, rba.PointerLocation, rba.StartTextAddress + rba.MetadataStartAddressOffset);
                }
                else
                {
                    locationKey = (rba.Prw.RenderFormat.File, rba.InitialStartTextAddress + rba.MetadataStartAddressOffset, rba.StartTextAddress + rba.MetadataStartAddressOffset);
                }

                if (DmpData.PointerLocationToid.ContainsKey(locationKey))
                {
                    var id = DmpData.PointerLocationToid[locationKey];
                    var containsId = false;

                    foreach (var x in rbr)
                    {
                        if (x.Value.Id == id)
                        {
                            containsId = true;
                            break;
                        }
                    }

                    rbr[depth].Id = id;

                    // if rbr already contains the id, stop reading to prevent inifinite loops.
                    if (containsId)
                    {
                        return;
                    }
                }
            }

            // Update "IdsSubentries" struct
            if (rba.LogMetadata && depth == 1)
            {
                var parentKey = rbr[0].Id;
                DmpData.SubentrMetadata[parentKey].IdsSubentries.Add(rbr[depth].Id);
            }

            DictionaryTables[] dictionaryInteractionType = rba.Prw.RenderFormat.DictionaryInteractions;
            bool reverseTableEndianness = rba.Prw.RenderFormat.ReverseTableEndianness;

            byte[] lookMeUp = null;
            long currentAddress = rba.StartTextAddress;
            int maxByteLength = Math.Max(Chara.BiggestDictionaryKeyArraySize, Chara.BiggestCHRKeyArraySize);

            if (rba.Prw.RenderFormat.DelimiterCount < 1)
            {
                throw new Exception($"ReadByteStreamWithPointerAndDelim requires a {nameof(rba.Prw.RenderFormat.DelimiterCount)} greater than 0.");
            }

            int delimiterCount = 0;

            if (rba.UsesDelimiter && (rba.Prw.RenderFormat.Delimiters == null || rba.Prw.RenderFormat.Delimiters.Length == 0))
            {
                throw new Exception($"ReadByteStreamWithPointerAndDelim requires the \"{nameof(rba.Prw.RenderFormat.Delimiters)}\" field in the settings file.");
            }

            if (!rba.UsesDelimiter && rba.ByteSize == Constants.GENERIC_NULL)
            {
                rba.ByteSize = rba.Buffer.Length;
            }

            if (rba.Buffer.Length == 0)
            {
                if (Global.Debug)
                {
                    Console.WriteLine("Buffer is empty.");
                }

                EndProcessingSubentry(rba, ref rbr, depth, true);
                return;
            }

            if (rba.ByteSize == 0)
            {
                EndProcessingSubentry(rba, ref rbr, depth, true);
                return;
            }

            if (rba.StartTextAddress < 0)
            {
                if (!Setting.Settings.Pointers.Read[rba.Key].PointerFormat.IgnoreOutOfBoundPointers)
                {
                    throw new Exception("Pointer value not positive with no override.");
                }

                EndProcessingSubentry(rba, ref rbr, depth, true);
                return;
            }

            if (rba.IncludeRedirects)
            {
                var key = (rba.Prw.RenderFormat.File, rba.InitialStartTextAddress + rba.MetadataStartAddressOffset);

                if (DmpData.SubentryLocations.ContainsKey(key))
                {
                    if (!DmpData.SubentryLocations[key].NotDumped)
                    {
                        // if two subentries with two different pointer locations reference 
                        // the same text location, indicate they're the same with the "SameAsId" tag.
                        if (DmpData.SubentryLocations[key].Id != rbr[depth].Id)
                        {
                            rbr[depth].SameAsId = DmpData.SubentryLocations[key].Id;
                            EndProcessingSubentry(rba, ref rbr, depth, true);
                            return;
                        }

                        /*
                         *  Continue reading.  With SameAs, the first dumped subenty (and its children) are deemed to be to be the "original".
                         *  while any future subentries with the same pointer and text location are deemed to be "sameas" / duplicates.
                         *  With this process, it's straightforward.
                         *  However, with sublabels, a string can be a subset of another without us knowing.
                         *  Its subentries will be dumped as well. Therefore, it's possible for other subentries to reference the label's subenties
                         *  by these ids. 
                         *  As a result, we dump these as well. The unfortunate side effect is that things are a bit out of order.
                         *  The only solution to this is to modify ids, which would require strings ids to be stored in an object just to store strings,
                         *  even though they're immutable.
                         *  https://stackoverflow.com/questions/6921097/if-strings-are-immutable-in-c-how-come-i-am-doing-this
                         *  
                         *  Additionally, a third pass would have to be done because ids would have to be updated within the dumped text.
                         *  
                         *  At the cost of subentries being broken up, from the parent, I'm going the simpler route.
                         */

                        if (/*DmpData.SubentryLocations[key].Id != rbr[depth].Id &&*/ DmpData.SubentryLocations[key].ParentAddress < rba.InitialStartTextAddress + rba.MetadataStartAddressOffset)
                        {
                            rbr[depth].IsSubentryLabel = true;
                            //EndProcessingSubentry(rba, ref rbr, depth, true);
                            //return;
                        }
                    }
                    else
                    {
                        //DmpData.SubentryLocations[key].NotDumped = false;
                        //rbr[depth].ScriptString += $"<label id=\"{DmpData.SubentryLocations[key].Id}\">";
                    }
                }
            }

            while (true)
            {
                if (currentAddress > rba.Buffer.Length)
                {
                    if (rba.Prw.PointerFormat.IgnoreOutOfBoundPointers)
                    {
                        EndProcessingSubentry(rba, ref rbr, depth, true);
                        return;
                    }

                    throw new Exception($"{nameof(currentAddress)}, {MyMath.DecToHex(currentAddress, Prefix.X)}, is outside file limits.");
                }

                if (currentAddress == rba.Buffer.Length)
                {
                    if (rba.UsesDelimiter)
                    {
                        throw new Exception($"{nameof(currentAddress)}, {MyMath.DecToHex(currentAddress, Prefix.X)}, delimeter not found.");
                    }

                    break;
                }

                void generateLabel(ref Dictionary<int, ReadByteStreamReturn> rbr)
                {
                    var usageKey = (rba.Prw.RenderFormat.File, currentAddress + rba.MetadataStartAddressOffset);

                    if (rba.InitialStartTextAddress == currentAddress)
                    {
                        return;
                    }

                    if (!DmpData.SubentryLocations.Keys.Contains(usageKey))
                    {
                        return;
                    }

                    var key = DmpData.SubentryLocations[usageKey].Id;

                    // This builds up the metadata on how text entries overlap one another
                    // This requires *two* dumping passes to get a complete picture.
                    if (rba.LogMetadata)
                    {
                        DmpData.SubentryLocations[usageKey].ParentAddress = Math.Min(DmpData.SubentryLocations[usageKey].ParentAddress, rba.InitialStartTextAddress + rba.MetadataStartAddressOffset);

                        if (!DmpData.SubentryLocations[usageKey].Entries.ContainsKey(rba.InitialStartTextAddress + rba.MetadataStartAddressOffset))
                        {
                            DmpData.SubentryLocations[usageKey].Entries.Add(rba.InitialStartTextAddress + rba.MetadataStartAddressOffset, rbr[depth]);
                        }
                    }

                    if (DmpData.PassNumber == 1)
                    {
                        if (/*rba.InitialStartTextAddress + rba.MetadataStartAddressOffset == DmpData.SubentryLocations[usageKey].ParentAddress*/ true)
                        {
                            rbr[depth].ScriptString += $"<label id=\"{key}\">";
                        }
                    }
                }

                void CheckSize(ref Dictionary<int, ReadByteStreamReturn> rbr, ref bool isOver)
                {
                    if (rba.ByteSize != Constants.GENERIC_NULL)
                    {
                        if (rba.ByteSize < 0)
                        {
                            throw new Exception($"{nameof(rba.ByteSize)} cannot be less than zero.");
                        }

                        if (rba.ByteSize == rbr[depth].ScriptBytes.Count)
                        {
                            rbr[depth].ScriptBytesWithoutDelimiter = rbr[depth].ScriptBytes;
                            isOver = true;
                        }

                        if (rba.ByteSize < rbr[depth].ScriptBytes.Count)
                        {
                            throw new Exception($"ReadByteStreamWithPointer: size exceeded the size limit of { MyMath.DecToHex(rba.ByteSize, Prefix.X) }. " +
                                $"Current size: { MyMath.DecToHex(rbr[depth].ScriptBytes.Count, Prefix.X) }");
                        }
                    }
                }

                generateLabel(ref rbr);

                if (rbr[depth].ScriptBytes.Count >= 10000000)
                {
                    throw new Exception($"The ScriptBytes count is {rbr[depth].ScriptBytes.Count}. " +
                        "It's getting rather long, so you're probably reading invalid data at this point.");
                }

                int lengthToTry = maxByteLength;
                while (!(currentAddress + lengthToTry <= rba.Buffer.Length))
                {
                    lengthToTry--;
                }

                DictionaryTables Found = DictionaryTables.Null;

                if (lengthToTry == 0)
                {
                    throw new Exception($"{nameof(lengthToTry)} cannot be zero.");
                }

                while (Found == DictionaryTables.Null && lengthToTry > 0)
                {
                    lookMeUp = new byte[lengthToTry];
                    Array.Copy(rba.Buffer, currentAddress, lookMeUp, 0, lengthToTry);

                    if (reverseTableEndianness)
                    {
                        lookMeUp = lookMeUp.Reverse().ToArray();
                    }

                    switch (dictionaryInteractionType)
                    {
                        case DictionaryTables[] a when a.Contains(DictionaryTables.Dictionary):
                        case DictionaryTables[] b when b.Contains(DictionaryTables.ControlCode):
                        case DictionaryTables[] c when c.Contains(DictionaryTables.ChrDictionary):
                            if (Chara.DictControlcode.ContainsKey(lookMeUp))
                            {
                                var regx = Chara.DictControlcode[lookMeUp].RegexRequirement;
                                if (regx == null)
                                {
                                    Found = DictionaryTables.ControlCode;
                                    goto Out;
                                }

                                if (regx.IsMatch(rbr[depth].ScriptString))
                                {
                                    Found = DictionaryTables.ControlCode;
                                    goto Out;
                                }
                            }

                            if (Chara.DictSortByKey.ContainsKey(lookMeUp) && Chara.DictSortByKey[lookMeUp].Length != 0)
                            {
                                Found = DictionaryTables.Dictionary;
                                goto Out;
                            }
                            break;
                        default:
                            break;
                    }

                    if (dictionaryInteractionType.Contains(DictionaryTables.CHR) && Chara.IsCHRValid(lookMeUp))
                    {
                        Found = DictionaryTables.CHR;
                        break;
                    }

                    lengthToTry--;
                }

                Out:

                if (lengthToTry == 0)
                {
                    lengthToTry = 1;
                }

                currentAddress += lengthToTry;

                if (lookMeUp == null)
                {
                    throw new Exception($"{nameof(lookMeUp)} cannot be null.");
                }

                bool isOver = false;

                if (Found != DictionaryTables.Null)
                {
                    string foundDictionaryValue;
                    int argumentLength = 0;

                    switch (Found)
                    {
                        case DictionaryTables.ControlCode:
                            foundDictionaryValue = Chara.DictControlcode[lookMeUp].StringRepresentation;
                            argumentLength = Chara.DictControlcode[lookMeUp].CodeArgumentLength;
                            var table = Chara.DictControlcode[lookMeUp].TableEntry;
                            var controlCodeAction = Chara.DictControlcode[lookMeUp].Action;
                            action = controlCodeAction == Enums.ControlCodeAction.None ? action : controlCodeAction;

                            if (table != "")
                            {
                                Chara.QuickChange(true, rba.Prw.RenderFormat.Tables[table]);
                            }
                            break;
                        case DictionaryTables.Dictionary:
                            foundDictionaryValue = Chara.GetDictionaryAsString(Chara.DictSortByKey[lookMeUp], dictionaryInteractionType, out _);
                            break;
                        case DictionaryTables.CHR:
                            foundDictionaryValue = Chara.GetCHRValue(lookMeUp);
                            break;
                        default:
                            throw new Exception($"Invalid dictionary table: {lookMeUp}");
                    }

                    if (string.IsNullOrEmpty(foundDictionaryValue))
                    {
                        throw new Exception("Found control code/dictionary/CHR value cannot be empty.");
                    }

                    // If we reversed the key, reverse it back.
                    if (reverseTableEndianness)
                    {
                        lookMeUp = lookMeUp.Reverse().ToArray();
                    }

                    switch (action)
                    {
                        case Enums.ControlCodeAction.DontPrintText:
                            if (Found == DictionaryTables.ControlCode)
                            {
                                // Embedded pointer control codes are important to print.
                                var result = rba.Prw.EmbeddedPointer.Formats.Where(s => s.ControlCode == foundDictionaryValue);
                                if (result.Any())
                                {
                                    goto case Enums.ControlCodeAction.PrintText;
                                }
                            }

                            rbr[depth].ScriptString += MyMath.FormatBytesInBrackets(lookMeUp);
                            break;
                        case Enums.ControlCodeAction.PrintText:
                            rbr[depth].ScriptString += foundDictionaryValue;
                            break;
                        default:
                            throw new Exception($"Invalid control code action: {action}");
                    }

                    //var delimiterMatch = false;

                    //foreach (var a in rba.Prw.RenderFormat.Delimiters)
                    //{
                    //    // Match with ends with for dictionary values with delimiters
                    //    // intended to be used in the parent (Tales of Phantasia SNES translation)
                    //    if (foundDictionaryValue.EndsWith(a))
                    //    {
                    //        delimiterMatch = true;
                    //    }
                    //}

                    if (rba.UsesDelimiter && rba.Prw.RenderFormat.Delimiters.Contains(foundDictionaryValue))
                    {
                        delimiterCount++;
                        if (rba.Prw.RenderFormat.DelimiterCount == delimiterCount)
                        {
                            isOver = true;
                            rbr[depth].ScriptBytesWithoutDelimiter = new List<byte>(rbr[depth].ScriptBytes);
                        }
                    }

                    rbr[depth].ScriptBytes = rbr[depth].ScriptBytes.Concat(lookMeUp).ToList();

                    CheckSize(ref rbr, ref isOver);

                    //generateLabel(ref rbr);

                    if (!EmbeddedPointerFormatCalc(ref rba, ref rbr, foundDictionaryValue, ref currentAddress, ref depth))
                    {
                        EndProcessingSubentry(rba, ref rbr, depth, true);
                        return;
                    }

                    while (argumentLength-- > Constants.CODE_ARGUMENT_LENGTH_NULL)
                    {
                        generateLabel(ref rbr);

                        if (rba.Buffer.Length > currentAddress)
                        {
                            rbr[depth].ScriptBytes.Add(rba.Buffer[currentAddress]);
                            rbr[depth].ScriptString += MyMath.FormatByteInBrackets(rba.Buffer[currentAddress++]);
                        }
                        else
                        {
                            throw new Exception($"String ends to early. Control code \"{ foundDictionaryValue }\" expected an argument.");
                        }
                    }

                    if (isOver)
                    {
                        break;
                    }
                }
                else
                {
                    rbr[depth].ScriptString += MyMath.FormatByteInBrackets(lookMeUp[0]);
                    rbr[depth].ScriptBytes.Add(lookMeUp[0]);

                    CheckSize(ref rbr, ref isOver);

                    if (isOver)
                    {
                        break;
                    }
                }
            }

            // If it's a subentry label, don't blank out the data as it will be blanked out again
            // We don't want to interfere with the dumping process for future entries.
            if (!rbr[depth].IsSubentryLabel)
            {
                EndProcessingSubentry(rba, ref rbr, depth, true);
            }
            else
            {
                EndProcessingSubentry(rba, ref rbr, depth, false);
            }
        }

        private static void EndProcessingSubentry(ReadByteStreamArgs rba, ref Dictionary<int, ReadByteStreamReturn> rbr, int depth, bool blankoutData)
        {
            LogMetadata(rba, ref rbr, depth);

            if (Global.Debug)
            {
                Console.WriteLine($"End of decompressing subentry #{depth}");
                Console.WriteLine($"Subentry #{depth}'s contents:\n{rbr[depth].ScriptString}");
            }

            if (blankoutData)
            {
                // Don't blank out the delimiter character becuase data can overlap
                // and make the editor read longer than it should.
                BlankOutData(rba.InitialStartTextAddress, rbr[depth].ScriptBytesWithoutDelimiter.Count, rba.Buffer);
            }
        }

        private static void LogMetadata(ReadByteStreamArgs rba, ref Dictionary<int, ReadByteStreamReturn> rbr, int depth)
        {
            if (rba.LogMetadata)
            {
                if (rba.Prw.PointerFormat.PointerGrouping != PointerGrouping.Single)
                {
                    var pointerLocationKey = (rba.Prw.RenderFormat.File, rba.PointerLocation, rba.StartTextAddress);
                    if (!DmpData.PointerLocationToid.ContainsKey(pointerLocationKey))
                    {
                        DmpData.PointerLocationToid.Add(pointerLocationKey, rbr[depth].Id);
                    }
                }
                else
                {
                    var textLocationKey = (rba.Prw.RenderFormat.File, rba.InitialStartTextAddress, rba.StartTextAddress);
                    if (!DmpData.PointerLocationToid.ContainsKey(textLocationKey))
                    {
                        DmpData.PointerLocationToid.Add(textLocationKey, rbr[depth].Id);
                    }
                }

                if (DmpData.SubentrMetadata.ContainsKey(rbr[depth].Id))
                {
                    DmpData.SubentrMetadata[rbr[depth].Id].IdMapping = rbr[depth];
                }

                var key = (rba.Prw.RenderFormat.File, rba.InitialStartTextAddress);
                if (!DmpData.SubentryLocations.Keys.Contains(key))
                {
                    DmpData.SubentryLocations.Add(key, new SubentryLocationsValue());
                    DmpData.SubentryLocations[key].ParentAddress = rba.InitialStartTextAddress;
                    DmpData.SubentryLocations[key].Id = rbr[depth].Id;
                }
            }

            // Sanity check
            if (rbr[depth].Id == rbr[depth].SameAsId)
            {
                throw new Exception($"SameAsId cannot be same as Id: {rbr[depth].Id}");
            }
        }

        private bool EmbeddedPointerFormatCalc(ref ReadByteStreamArgs rba, ref Dictionary<int, ReadByteStreamReturn> rbr, string controlCode, ref long currentAddress, ref int depth)
        {
            bool continueAfterReading = true;
            List<EmbeddedPointerFormat> embedList = new List<EmbeddedPointerFormat>();

            if (rba.Prw.EmbeddedPointer == null)
            {
                return continueAfterReading;
            }

            foreach (var pointerFormat in rba.Prw.EmbeddedPointer.Formats)
            {
                if (pointerFormat.RecursionLimit != Constants.GENERIC_NULL && depth >= pointerFormat.RecursionLimit)
                {
                    break;
                }

                if (controlCode == pointerFormat.ControlCode)
                {
                    embedList.Add(pointerFormat.Clone());
                }
            }

            if (embedList.Count == 0)
            {
                return continueAfterReading;
            }

            foreach (var e in embedList)
            {
                if (Global.Debug)
                {
                    Console.WriteLine($"Managing subentry #{depth}'s embedded pointer: {controlCode}; {nameof(currentAddress)}: {MyMath.DecToHex(currentAddress, Prefix.X)}");
                }

                Restart:
                for (int embedPointerNo = 0; embedPointerNo < e.EntryNumber; embedPointerNo++)
                {
                    if (e.EntryNumber > 1)
                    {

                    }
                    byte[] pointerBytes = new byte[e.PointerLength];

                    if (pointerBytes.Length != 0)
                    {
                        Array.Copy(FILE.DATA, (int)currentAddress, pointerBytes, 0, pointerBytes.Length);
                    }

                    if (rba.Buffer.Length > currentAddress)
                    {
                        rbr[depth].ScriptBytes = rbr[depth].ScriptBytes.Concat(pointerBytes).ToList();
                    }

                    long pointerAddress = -1;

                    if (currentAddress == 0x00000000000008d3)
                    {

                    }

                    switch (e.PointerType)
                    {
                        case Enums.PointerType.None:
                            pointerAddress = currentAddress;
                            break;
                        case Enums.PointerType.LittleEndian:
                            PointerManager.ReadLittleEndian(pointerBytes, out pointerAddress);
                            break;
                        case Enums.PointerType.BigEndian:
                            PointerManager.ReadBigEndian(pointerBytes, out pointerAddress);
                            break;
                        case Enums.PointerType.EarthboundEmbeddedPointer:
                            // Earthbound embedded pointers contain the amount of pointers there are in the first byte after the control code.
                            // Once we've determined the amount of pointers there are, restart.
                            // Probably worth refactoring this so it's more flexible and supports stuff like big endian (and without the goto).
                            e.EntryNumber = pointerBytes[0];
                            e.PointerType = Enums.PointerType.LittleEndian;
                            embedPointerNo = -1;

                            rbr[depth].ScriptBytes.Add(pointerBytes[0]);
                            rbr[depth].ScriptString += MyMath.FormatByteInBrackets(pointerBytes[0]);

                            currentAddress++;
                            goto Restart;
                        case Enums.PointerType.Custom:
                            try
                            {
                                if (e.CustomPointerFormat == null)
                                {
                                    throw new Exception($"{nameof(e.CustomPointerFormat)} cannot be null");
                                }

                                if (e.CustomPointerFormat.Length != e.PointerLength)
                                {
                                    throw new Exception($"{nameof(e.CustomPointerFormat)}.Length and {e.PointerLength} must be the same: {e.CustomPointerFormat.Length} != {e.PointerLength}");
                                }

                                PointerManager.GetAddressCustomFormat(pointerBytes, e.CustomPointerFormat, out pointerAddress);
                            }
                            catch (Exception ee)
                            {
                                throw new Exception($"{ee.Message};\nCurrent entry text: {rbr[depth].ScriptString}");
                            }
                            break;
                        default:
                            throw new Exception($"Invalid {nameof(e.PointerType)}: {e.PointerType}");
                    }

                    if (e.AddressType == Enums.AddressType.Relative)
                    {
                        var mask = 0x80 << ((e.PointerLength - 1) * 8);
                        var maskNegative = 0xffffffff >> ((8 - e.PointerLength) * 8);

                        if ((mask & pointerAddress) != 0x0)
                        {
                            pointerAddress ^= maskNegative ^ -1;
                        }
                    }

                    pointerAddress += e.PcDifference;

                    switch (e.AddressType)
                    {
                        case Enums.AddressType.Absolute:
                            break;
                        case Enums.AddressType.Relative:
                            pointerAddress += currentAddress;
                            break;
                        default:
                            throw new Exception($"Invalid {nameof(e.AddressType)}: {e.AddressType}");
                    }

                    if (e.PcDifferenceExcludePointer)
                    {
                        if (pointerAddress >= currentAddress)
                        {
                            pointerAddress += (e.EntryNumber - embedPointerNo) * e.PointerLength;
                        }
                        else
                        {
                            pointerAddress += (embedPointerNo - e.EntryNumber) * e.PointerLength;
                        }
                    }

                    bool isValidPcAddress = true;

                    switch (e.AddressConversion)
                    {
                        case Enums.AddrConversion.None:
                            break;
                        case Enums.AddrConversion.LoROM1:
                            pointerAddress = AddressLoROM1.SnesToPc((int)pointerAddress, out isValidPcAddress);
                            break;
                        case Enums.AddrConversion.LoROM2:
                            pointerAddress = AddressLoROM2.SnesToPc((int)pointerAddress, out isValidPcAddress);
                            break;
                        case Enums.AddrConversion.HiROM:
                            pointerAddress = AddressHiRom.SnesToPc((int)pointerAddress, out isValidPcAddress);
                            break;
                        case Enums.AddrConversion.ExLoROM:
                            pointerAddress = AddressExLoROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                            break;
                        case Enums.AddrConversion.ExHiROM:
                            pointerAddress = AddressExHiROM.SnesToPc((int)pointerAddress, out isValidPcAddress);
                            break;
                        default:
                            throw new Exception($"Invalid {nameof(e.AddressConversion)}: {e.AddressConversion}");
                    }

                    if (!isValidPcAddress)
                    {
                        throw new Exception("Subentry address conversion failed.");
                    }

                    var newRba = rba.Clone();
                    newRba.StartTextAddress = newRba.InitialStartTextAddress = pointerAddress;

                    // There's cases where you care about the ordering of strings and want to write them in the right order. 
                    // So you have the option to skip dumping an embedded pointer. You'll want to carve the data instead to grab this data.
                    if (e.DontDump)
                    {
                        var key = (rba.Prw.RenderFormat.File, newRba.InitialStartTextAddress + newRba.MetadataStartAddressOffset);

                        var subentryId = "";
                        if (rba.LogMetadata && !DmpData.SubentryLocations.Keys.Contains(key))
                        {
                            /*DmpData.SubentryLocations.Add(key, new SubentryLocationsValue());
                            DmpData.SubentryLocations[key].ParentAddress = rba.InitialStartTextAddress;
                            DmpData.SubentryLocations[key].Id = rbr[depth].Id;*/

                            // We're just using the object for the id
                            subentryId = new ReadByteStreamReturn(rba.Prw.PointerFormat.SubentryKeyPrefix).Id;
                            DmpData.SubentrMetadata.Add(subentryId, new());
                            DmpData.SubentrMetadata[subentryId].EmbeddedPointerFormat = e;
                            //DmpData.SubentrMetadata[rbr[depth].Id].IdsSubentries.Add(subentryId);

                            DmpData.SubentryLocations.Add(key, new SubentryLocationsValue());
                            DmpData.SubentryLocations[key].ParentAddress = rba.InitialStartTextAddress;
                            DmpData.SubentryLocations[key].Id = subentryId;
                            DmpData.SubentryLocations[key].NotDumped = true;
                        }
                        else
                        {
                            subentryId = DmpData.SubentryLocations[key].Id;
                        }

                        currentAddress += e.PointerLength;

                        rbr[depth].ScriptString += $"<subentryPointer id=\"{subentryId}\">";

                        continue;
                    }

                    // Create a new entry. The "depth" key will be the largest key + 1, to avert duplicate key insertion conflicts.
                    // Create a new rbr, which will contain the text metadata and start reading data at the location indicated in StartAddress
                    var newDepthKey = rbr.Keys.Max() + 1;

                    rbr.Add(newDepthKey, new ReadByteStreamReturn(rba.Prw.PointerFormat.SubentryKeyPrefix));
                    rbr[newDepthKey].TextLocation = pointerAddress;

                    continueAfterReading = e.ContinueAfterProcessing;

                    rbr[depth].ScriptBytesWithoutDelimiter = rbr[depth].ScriptBytes;

                    EndProcessingSubentry(rba, ref rbr, depth, !e.ContinueAfterProcessing);

                    if (rba.LogMetadata && !DmpData.SubentrMetadata.ContainsKey(rbr[newDepthKey].Id))
                    {
                        DmpData.SubentrMetadata.Add(rbr[newDepthKey].Id, new());
                        DmpData.SubentrMetadata[rbr[newDepthKey].Id].EmbeddedPointerFormat = e;
                    }

                    ReadByteStreamWithPointerRecursive(newRba, ref rbr, newDepthKey);

                    switch (e.PointerType)
                    {
                        case Enums.PointerType.None:
                            // If the entry has the same pointer as another entry, it won't be read. So the byte count will be zero.
                            // To ensure we get the actual byte count, reference the id and get the entry from SameAs.IdMapping
                            currentAddress += DmpData.SubentrMetadata[GetId(rbr, newDepthKey)].IdMapping.ScriptBytes.Count;
                            break;
                        case Enums.PointerType.LittleEndian:
                        case Enums.PointerType.BigEndian:
                        case Enums.PointerType.Custom:

                            for (int k = 0; k < e.PointerLength; k++)
                            {
                                rbr[depth].ScriptBytes.Add(FILE.DATA[currentAddress]);
                                currentAddress++;
                            }
                            break;
                        default:
                            throw new Exception($"Invalid {nameof(e.PointerType)}: {e.PointerType}");
                    }

                    var entryId = GetId(rbr, newDepthKey);

                    rbr[depth].ScriptString += $"<subentryPointer id=\"{entryId}\">";
                }
            }

            return continueAfterReading;
        }

        private static string GetId(Dictionary<int, ReadByteStreamReturn> rbr, int depth)
        {
            if (!rbr.ContainsKey(depth))
            {
                throw new Exception($"ReadByteStreamReturn doesn't contain the key {depth}");
            }
            // return rbr[depth].SameAsId == null ? rbr[depth].Id : rbr[depth].SameAsId;
            return rbr[depth].SameAsId ?? rbr[depth].Id;
        }
        public Dictionary<int, ReadByteStreamReturn> ReadByteMarioPicrossSNES(long entryNo, long pos, string key, PointerReadWrite prw, long pointerLocation)
        {
            const int depth = 0;

            Dictionary<int, ReadByteStreamReturn> ret;

            // Start of logic at c01809 in SNES memory
            int StackValue = 0x4;
            List<byte> byteList = new List<byte>();
            string scriptText = "";

            while (true)
            {
                byte b = FILE.DATA[pos];
                byteList.Add(b);
                pos++;
                /*
                 * c0181a cmp $01,s     [001fb9]
                 * c0181c beq $1832     [c01832]
                 */
                if (b == StackValue)
                {
                    int puzzleNameLength = ((FILE.DATA[pos + 1] << 8) + FILE.DATA[pos]) - 4;

                    byteList.Add(FILE.DATA[pos++]);
                    byteList.Add(FILE.DATA[pos++]);
                    scriptText += MyMath.FormatBytesInBrackets(byteList.ToArray());

                    byte[] picrossPuzzleName = new byte[puzzleNameLength];
                    Array.Copy(FILE.DATA, pos, picrossPuzzleName, 0, puzzleNameLength);

                    // Get the puzzle name
                    ret = ReadByteStreamWithPointer(new ReadByteStreamArgs(
                        entryNo,
                        0,
                        key,
                        pointerLocation,
                        prw,
                        picrossPuzzleName,
                        IncludeRedirects: false,
                        UsesDelimiter: false,
                        ByteSize: picrossPuzzleName.Length));
                    scriptText += ret[depth].ScriptString;

                    pos += picrossPuzzleName.Length;
                    byteList = byteList.Concat(ret[depth].ScriptBytes).ToList();

                    // Read the rest of the puzzle data
                    ret = ReadByteStreamWithPointer(new ReadByteStreamArgs(
                        entryNo,
                        pos,
                        key,
                        pointerLocation,
                        prw,
                        FILE.DATA,
                        IncludeRedirects: false,
                        UsesDelimiter: true
                        ));
                    scriptText += MyMath.FormatBytesInBrackets(ret[depth].ScriptBytes.ToArray());

                    ret[depth].ScriptBytes = byteList.Concat(ret[depth].ScriptBytes).ToList();
                    ret[depth].ScriptBytesWithoutDelimiter = byteList.Concat(ret[depth].ScriptBytesWithoutDelimiter).ToList();
                    ret[depth].ScriptString = scriptText;

                    break;
                }

                int i = FILE.DATA[pos];

                while (--i >= 0)
                {
                    byteList.Add(FILE.DATA[pos++]);
                }
            }

            return ret;
        }

        public Dictionary<int, ReadByteStreamReturn> ReadByteTensaiBakabon(ReadByteStreamArgs rba)
        {
            rba.Prw.RenderFormat.DelimiterCount = FILE.DATA[rba.StartTextAddress];
            Dictionary<int, ReadByteStreamReturn> rbr = new Dictionary<int, ReadByteStreamReturn>
            {
                { 0, new ReadByteStreamReturn(rba.Prw.PointerFormat.SubentryKeyPrefix) }
            };

            rbr[0].ScriptBytesWithoutDelimiter = rbr[0].ScriptBytes = new byte[] { (byte)rba.Prw.RenderFormat.DelimiterCount }.ToList();
            rbr[0].ScriptString = MyMath.FormatByteInBrackets(FILE.DATA[rba.StartTextAddress++]);

            return ReadByteStreamWithTextLocation(rba, rbr);
        }

        private static void BlankOutData(long startAddress, long byteSize, byte[] buffer)
        {
            if (Setting.Settings.Misc.BlankOutTextDataAfterRead)
            {
                if (DmpData.PassNumber == 1)
                {
                    for (long i = startAddress; i < startAddress + byteSize; i++)
                    {
                        buffer[i] = Setting.Settings.Misc.BlankOutByte;
                    }
                }
            }
        }
    }
}